package com.androidp.mc_trainer.views

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.androidp.mc_trainer.R

/**
 * Describes the current status of the user login
 */
enum class LoginStatus {
    INIT,
    PASSWORD_EMPTY,
    EMAIL_EMPTY_OR_INCORRECT,
    NOT_REGISTERED,
    ERROR,
    FAILED,
    SUCCESS
}

/**
 * Displays the login page
 *
 * @see LoginFragment
 */
class LoginActivityView : AppCompatActivity() {
    /**
     * Called when the activity is created
     *
     * @param savedInstanceState
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        supportActionBar?.setBackgroundDrawable(getDrawable(R.color.thm_green))
        supportFragmentManager.commit {
            replace<LoginFragment>(R.id.container)
        }
    }
}