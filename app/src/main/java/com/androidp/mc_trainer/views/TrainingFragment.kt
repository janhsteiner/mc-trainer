package com.androidp.mc_trainer.views

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.GridLayout
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.recyclerview.widget.RecyclerView
import com.androidp.mc_trainer.R
import com.androidp.mc_trainer.models.Application
import com.androidp.mc_trainer.models.entities.Question
import com.androidp.mc_trainer.viewmodels.TrainingViewModel
import com.androidp.mc_trainer.viewmodels.TrainingViewModelFactory
import com.androidp.mc_trainer.views.utils.AchievementsEnum
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.max

/**
 * Implements the training mode UI
 */
class TrainingFragment : Fragment() {
    private val answerButtons = mutableListOf<Button>()
    private val answers = mutableListOf<Pair<String, Boolean>>()
    private val markedCorrect = arrayOf(false, false, false, false, false, false)

    private lateinit var layout: ConstraintLayout
    private lateinit var questionNumberText: TextView
    private lateinit var questionText: TextView

    private val questionList: MutableList<Question> = mutableListOf()
    private val questionProgress = mutableListOf<QuestionProgress>()
    private lateinit var questionProgressView: RecyclerView

    private var questionIndex = 0

    private var numCorrectAnswers = 0
    private val correctAnswers = mutableListOf<Int>()
    private val wrongAnswers = mutableListOf<Int>()

    private var currentQuestion: Question? = null

    private var correctlyAnsweredInSuccession = 0
    private var maxCorrectlyAnsweredInSuccession = 0

    private var questionDone = false

    private var trainingId = 0

    private lateinit var trainingViewModel: TrainingViewModel

    private lateinit var fab: FloatingActionButton

    /**
     * Called when the fragment is created
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // hide actionbar
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        (activity as MainActivityView).drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_training, container, false)
    }

    /**
     * Called when the view is inflated
     *
     * @param inflater
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        trainingId = arguments?.getInt("trainingId", 0) ?: 0

        val trainingViewModel: TrainingViewModel by viewModels {
            return@viewModels with(activity?.application as Application) {
                TrainingViewModelFactory(
                    this.questionRepository,
                    this.trainingRepository,
                    pusRepository,
                    trainingId
                )
            }
        }

        this.trainingViewModel = trainingViewModel

        layout = view.findViewById(R.id.training_layout)
        layout.visibility = View.GONE
        questionText = view.findViewById(R.id.training_question)

        questionProgressView = view.findViewById(R.id.progressIndicators)
        questionProgressView.adapter = QuestionProgressAdapter(requireContext())

        /* Find answer buttons */
        answerButtons.clear()
        answerButtons.addAll(
            view.findViewById<GridLayout>(R.id.answers_grid)
                .children
                .filterIsInstance<Button>()
                .toList()
        )

        /* Setup click listeners */
        answerButtons.forEachIndexed { index, button ->
            button.setOnClickListener { _ ->
                onAnswerSelected(button, index)
            }
        }

        view.findViewById<TextView>(R.id.training_question).setOnClickListener {
            if (questionDone) {
                gotoNextQuestion()
            }
        }

        /* Get questions from view model and set up data */
        trainingViewModel.getRandomizedQuestions().observe(viewLifecycleOwner) {
            trainingViewModel.getRandomizedQuestions().removeObservers(this)
            if (questionList.isEmpty()) {
                questionList.clear()
                questionList.addAll(it)

                if (questionList.isEmpty()) {
                    AlertDialog.Builder(context)
                        .setTitle(R.string.training_completed_title)
                        .setMessage(R.string.training_completed_message)
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton("Ok") { _, _ ->
                            // Back to dashboard
                            val i = Intent(context, MainActivityView::class.java)
                            i.putExtra("intendedFragment", "dashboard")
                            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            activity?.applicationContext?.startActivity(i)
                        }
                        .show()
                } else {
                    questionList.forEach { question ->
                        questionProgress.add(QuestionProgress(question.id))
                    }

                    (questionProgressView.adapter as QuestionProgressAdapter).notifyDataSetChanged()
                    layout.visibility = View.VISIBLE

                    questionIndex = 0
                    loadNextQuestion()
                }
            }
        }

        /* Setup back button */
        view.findViewById<ImageButton>(R.id.back_button).setOnClickListener {
            onBackPressed()
        }

        /* Setup floating action button */
        fab = view.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            if (!questionDone) {
                submitAnswers()
            } else {
                gotoNextQuestion()
            }
        }
    }

    /**
     * Called when the user presses the back button or presses the back button in the toolbar
     */
    fun onBackPressed() {
        AlertDialog.Builder(context)
            .setTitle(R.string.training_leave_title)
            .setMessage(R.string.training_leave_message)
            // The dialog is automatically dismissed when a dialog button is clicked.
            .setPositiveButton(R.string.yes) { _, _ ->
                // Back to dashboard
                val intent = Intent(context, MainActivityView::class.java)
                intent.putExtra("intendedFragment", "dashboard")
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                activity?.applicationContext?.startActivity(intent)
            }
            .setNegativeButton(R.string.no) { dialog, _ -> dialog.dismiss() }
            .setIcon(R.drawable.ic_baseline_warning_24)
            .show()
    }

    /**
     * Loads the next question to be displayed
     */
    private fun loadNextQuestion() {
        val question = questionList[questionIndex]
        currentQuestion = question

        val answers = mutableListOf<Pair<String, Boolean>>()
        answers.add(question.answer1 to question.correct1)
        answers.add(question.answer2 to question.correct2)
        answers.add(question.answer3 to question.correct3)
        answers.add(question.answer4 to question.correct4)
        if (question.answer5?.isNotBlank() == true) {
            answers.add(question.answer5!! to question.correct5!!)
        }
        if (question.answer6?.isNotBlank() == true) {
            answers.add(question.answer6!! to question.correct6!!)
        }

        markedCorrect.forEachIndexed { index, _ ->
            markedCorrect[index] = false
        }

        /* Set question text */
        questionText.text = question.question
        if (questionText.text.count() > 35) {
            questionText.textSize = 15.0f
        } else {
            questionText.textSize = 32.0f
        }

        /* Reset BG */
        activity?.let { ContextCompat.getColor(it.applicationContext, R.color.white) }?.let {
            layout.setBackgroundColor(
                it
            )
        }

        /* Reset buttons */
        answerButtons.forEach {
            it.setBackgroundResource(R.drawable.button_answer_unanswered)
            it.setTextColor(Color.WHITE)
            it.visibility = View.VISIBLE
        }

        fab.isEnabled = false

        /* Load answers */
        answers.forEachIndexed { index, pair ->
            val button = answerButtons[index]
            button.text = pair.first
            if (button.text.count() > 10) {
                button.textSize = 10.0f
            } else {
                button.textSize = 15.0f
            }
        }

        /* Hide surplus buttons */
        if (answers.count() < answerButtons.count()) {
            answerButtons.forEachIndexed { index, button ->
                if (index >= answers.count()) {
                    button.visibility = View.INVISIBLE
                }
            }
        }

        /* Set progress indicator to current */
        val progress = questionProgress.find { it.questionId == currentQuestion!!.id }!!
        progress.progress = when (progress.progress) {
            QuestionProgressType.INCORRECT -> QuestionProgressType.CURRENT_WRONG
            else -> QuestionProgressType.CURRENT_MISSING
        }

        questionProgressView.adapter?.notifyDataSetChanged()

        numCorrectAnswers = 0
        this.answers.clear()
        this.answers.addAll(answers.toList())
        questionDone = false
    }

    /**
     * Called when the user presses a certain answer
     *
     * @param button The answer button
     * @param num The number of the answer
     */
    private fun onAnswerSelected(button: Button, num: Int) {
        if (questionDone) {
            return
        }

        // Mark answer button
        val answerIndex = answerButtons.indexOf(button)
        if (markedCorrect[answerIndex]) {
            // Unmark
            button.setBackgroundResource(R.drawable.button_answer_unanswered)
            markedCorrect[answerIndex] = false
        } else {
            // Mark
            button.setBackgroundResource(R.drawable.button_answer_marked)
            markedCorrect[answerIndex] = true
        }

        // Enable or disable FAB depending on if any answer is marked
        fab.isEnabled = markedCorrect.any { it }
    }

    /**
     * Called when the user clicks the FAB to submit their answers
     */
    private fun submitAnswers() {
        var correct = true
        answers.forEachIndexed { index, pair ->
            if (pair.second != markedCorrect[index]) {
                correct = false
                return@forEachIndexed
            }
        }

        finishQuestion(correct)
    }

    /**
     * Finishes the current question, shows the true answers and changes the background depending on the result
     *
     * @param correct Did the user answer correctly?
     */
    private fun finishQuestion(correct: Boolean) {
        val bgColor = when (correct) {
            true -> R.color.correct_bg
            false -> R.color.incorrect_bg
        }

        activity?.let {
            ContextCompat.getColor(it.applicationContext, bgColor)
        }?.let {
            layout.setBackgroundColor(
                it
            )
        }

        if (!correct) {
            val shakeAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.shake_wrong)
            questionText.startAnimation(shakeAnimation)
            trainingViewModel.resetScoreForQuestionIds(wrongAnswers)
        }

        /* Reveal all other answers */
        answers.forEachIndexed { index, pair ->
            if (pair.second) {
                answerButtons[index].setBackgroundResource(R.drawable.button_answer_correct)
            } else {
                answerButtons[index].setBackgroundResource(R.drawable.button_answer_wrong)
            }
        }

        val progress = questionProgress.find { it.questionId == currentQuestion!!.id }!!

        if (correct) {
            if (!correctAnswers.contains(currentQuestion!!.id) &&
                !wrongAnswers.contains(currentQuestion!!.id)
            ) {
                correctAnswers.add(currentQuestion!!.id)
            }

            progress.progress = QuestionProgressType.CORRECT

            correctlyAnsweredInSuccession++
            maxCorrectlyAnsweredInSuccession =
                max(maxCorrectlyAnsweredInSuccession, correctlyAnsweredInSuccession)
        } else {
            if (!wrongAnswers.contains(currentQuestion!!.id)) {
                wrongAnswers.add(currentQuestion!!.id)
            }
            progress.progress = QuestionProgressType.INCORRECT
            questionList.add(currentQuestion!!)
            correctlyAnsweredInSuccession = 0
        }

        questionProgressView.adapter?.notifyDataSetChanged()

        questionDone = true
    }

    /**
     * Goes to the next question. If this is the last question, go straight to the results page.
     *
     * @see TrainingResultsFragment
     */
    private fun gotoNextQuestion() {
        questionIndex++
        if (questionIndex >= questionList.count()) {
            updateAchievements()

            /* update scores */
            lifecycleScope.launch(Dispatchers.IO) {
                trainingViewModel.increaseScoreForQuestionIds(correctAnswers)
                trainingViewModel.resetScoreForQuestionIds(wrongAnswers)

                /* Go to the summary page */
                withContext(Dispatchers.Main) {

                    val i = Intent(context, MainActivityView::class.java)
                    i.putExtra("intendedFragment", "trainingResults")
                    i.putExtra("trainingResultData", Bundle().apply {
                        putInt("trainingId", trainingId)
                        putInt("numCorrectlyAnswered", correctAnswers.count())
                        putInt("numIncorrectlyAnswered", wrongAnswers.count())
                    })

                    i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    activity?.applicationContext?.startActivity(i)
                    activity?.finish()
                }
            }
        } else {
            loadNextQuestion()
        }
    }

    /**
     * Updates achievements
     */
    private fun updateAchievements() {
        val options = requireActivity().getSharedPreferences("achievements", Context.MODE_PRIVATE)
        with(options.edit()) {
            putBoolean("finishedTraining", true)
            putInt("numCorrectlyAnswered", correctAnswers.count())
            putInt("numIncorrectlyAnswered", wrongAnswers.count())
            putInt("numMaxCorrectlyAnsweredInSuccession", maxCorrectlyAnsweredInSuccession)
            apply()
        }

        val userId = FirebaseAuth.getInstance().currentUser!!.uid
        AchievementsEnum.checkAchievements(options, userId)
    }

    /**
     * Represents the progress of a question during the training phase.
     */
    private enum class QuestionProgressType {
        NOT_ANSWERED,
        CORRECT,
        INCORRECT,
        CURRENT_MISSING,
        CURRENT_WRONG
    }

    /**
     * Represents the state of a question during the training phase, represented with a "point" in the title bar
     */
    private class QuestionProgress(
        val questionId: Int,
        var progress: QuestionProgressType = QuestionProgressType.NOT_ANSWERED
    )


    /* -- Question Progress RecyclerView adapter -- */
    /* Following two classes are concerned with the question progress indicators */

    /**
     * Implements the ViewHolder for the question progress indicator
     */
    private class QuestionProgressHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val questionIndicator: View = itemView.findViewById(R.id.questionIndicator)

        /**
         * Sets the question progress
         *
         * @param progress The question progress
         */
        fun setProgress(progress: QuestionProgressType) {
            val background = when (progress) {
                QuestionProgressType.NOT_ANSWERED -> R.drawable.indicator_missing
                QuestionProgressType.CORRECT -> R.drawable.indicator_correct
                QuestionProgressType.INCORRECT -> R.drawable.indicator_wrong
                QuestionProgressType.CURRENT_MISSING -> R.drawable.indicator_missing_current
                QuestionProgressType.CURRENT_WRONG -> R.drawable.indicator_wrong_current
            }
            questionIndicator.setBackgroundResource(background)
        }
    }

    /**
     * Implements the Adapter for the question progress indicator
     */
    private inner class QuestionProgressAdapter(val context: Context) :
        RecyclerView.Adapter<QuestionProgressHolder>() {

        /**
         * Called when the adapter needs to create a ViewHolder for an item
         *
         * @param parent
         * @param viewType
         */
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionProgressHolder {
            val inflater = LayoutInflater.from(parent.context)
            val view = inflater.inflate(R.layout.item_progress_indicator, parent, false)
            return QuestionProgressHolder(view)
        }

        /**
         * Returns the item count
         *
         * @return item count
         */
        override fun getItemCount(): Int = questionProgress.count()

        /**
         * Called when the adapter should bind a view holder
         *
         * @param holder    The view holder
         * @param position  The item position
         */
        override fun onBindViewHolder(holder: QuestionProgressHolder, position: Int) {
            val questionProgress = questionProgress[position]
            holder.setProgress(questionProgress.progress)
        }
    }
}