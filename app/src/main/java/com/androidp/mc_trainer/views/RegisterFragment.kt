package com.androidp.mc_trainer.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.androidp.mc_trainer.R
import com.androidp.mc_trainer.viewmodels.RegisterViewModel
import com.androidp.mc_trainer.viewmodels.RegisterViewModelFactory
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Represents the status of a user registration
 */
enum class RegisterStatus {
    /**
     * Registration was successful
     */
    SUCCESS,

    /**
     * User with this info already exists
     */
    EXISTS,

    /**
     * Failure to register
     */
    FAILURE
}

/**
 * Displays the registration page
 */
class RegisterFragment : Fragment() {
    /**
     * Called when the fragment is created
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    /**
     * Called when the view is created
     *
     * @param view
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel: RegisterViewModel by viewModels {
            RegisterViewModelFactory()
        }

        val email = view.findViewById<EditText>(R.id.email)
        val password = view.findViewById<EditText>(R.id.password)
        val passwordConfirmation = view.findViewById<EditText>(R.id.confirm_password)

        view.findViewById<Button>(R.id.register).setOnClickListener {
            email.error = when {
                email.text.toString().isBlank() -> {
                    getString(R.string.notification_email_empty)
                }
                !isEmailValid(email.text.toString()) -> {
                    getString(R.string.notification_email_empty)
                }
                else -> null
            }

            password.error = when {
                password.text.toString().isBlank() -> {
                    getString(R.string.invalid_password)
                }
                password.text.toString().length < 6 -> {
                    getString(R.string.password_too_short)
                }
                else -> null
            }

            passwordConfirmation.error = when {
                password.text.toString() != passwordConfirmation.text.toString() -> {
                    getString(R.string.passwords_do_not_match)
                }
                else -> null
            }

            if (email.error == null && password.error == null && passwordConfirmation.error == null) {
                viewModel.register(email.text.toString(), password.text.toString())
                viewModel.registerStatus.observe(viewLifecycleOwner) { _ ->
                    // disable register button to prevent multiple requests
                    it.isEnabled = false
                    val snack = Snackbar.make(
                        it, "",
                        Snackbar.LENGTH_INDEFINITE
                    )
                    snack.animationMode = Snackbar.ANIMATION_MODE_SLIDE
                    when (viewModel.registerStatus.value) {
                        RegisterStatus.EXISTS -> {
                            snack.setText(R.string.register_exists)
                            snack.show()
                            it.isEnabled = true
                        }
                        RegisterStatus.FAILURE -> {
                            snack.setText(R.string.register_failure)
                            snack.show()
                            it.isEnabled = true
                        }
                        RegisterStatus.SUCCESS -> {
                            // activity will be finished when user clicks acknowledges the
                            // alert dialog
                            showSuccessDialog()
                        }
                    }
                }
            }
        }

        email.apply {
            this.setText(viewModel.email)

            addTextChangedListener {
                viewModel.email = this.text.toString()
            }
        }

        password.apply {
            this.setText(viewModel.password)

            addTextChangedListener {
                viewModel.password = this.text.toString()
            }
        }

        passwordConfirmation.apply {
            this.setText(viewModel.passwordConfirmation)

            addTextChangedListener {
                viewModel.passwordConfirmation = this.text.toString()
            }
        }

        /* Back button */
        view.findViewById<ImageButton>(R.id.back_button).setOnClickListener {
            gotoLoginFragment()
        }
    }

    /**
     * Checks if the provided string is a valid email address.
     * The email is considered valid if
     *
     *     1. it is not empty or consists solely of whitespaces
     *     2. if it follows the email pattern (e.g. something@example.test)
     *
     * @param email the email string, which should be checked
     * @return true if the given string is a valid email string, else false
     */
    private fun isEmailValid(email: String?): Boolean {
        if (email == null || email.isBlank())
            return false

        val p: Pattern = Pattern.compile("^(\\w+.)*\\w+@(\\w+)(.\\w+)+\$")
        val m: Matcher = p.matcher(email)
        return m.matches()
    }

    /**
     * Shows an AlertDialog which informs the user about his/her successful registration.
     * After the user acknowledges this piece of information, the activity will be finished and
     * the user will be redirected to the login screen.
     */
    private fun showSuccessDialog() {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.register_success_title)
            .setMessage(R.string.register_success)
            .setPositiveButton(R.string.register_success_positive)
            { dialog, _ ->
                dialog.dismiss()
                gotoLoginFragment()
            }
            .show()
    }

    /**
     * Goes to the login screen
     */
    private fun gotoLoginFragment() {
        parentFragmentManager.commit {
            replace<LoginFragment>(R.id.container)
        }
    }
}