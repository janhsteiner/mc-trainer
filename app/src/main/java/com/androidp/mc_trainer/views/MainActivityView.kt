package com.androidp.mc_trainer.views

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.androidp.mc_trainer.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_dashboard.*

/**
 * Represents the main activity. Displays other app sections through fragments.
 */
class MainActivityView : AppCompatActivity() {
    private lateinit var catFrag: CatalogueFragment
    private lateinit var catQuestionFrag: CatalogueQuestionViewFragment
    val user = FirebaseAuth.getInstance().currentUser!!.uid
    private lateinit var toggle: ActionBarDrawerToggle

    /**
     * Called when a menu option is selected
     *
     * @param item the menu item
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Called when the activity is created
     *
     * @param savedInstanceState
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        title = "Wähle ein Training aus"

        // setup drawerMenu
        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        supportActionBar?.setBackgroundDrawable(getDrawable(R.color.thm_green))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        navView.setNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.item_achievements -> {
                    drawerLayout.closeDrawer(navView,true)
                    supportFragmentManager.commit {
                        replace<AchievementsFragment>(R.id.container)
                    }
                }
                R.id.item_dashboard -> {
                    drawerLayout.closeDrawer(navView,true)
                    supportFragmentManager.commit {
                        replace<DashboardFragment>(R.id.container)
                    }
                }
                R.id.item_logout -> {
                    drawerLayout.closeDrawer(navView,true)
                    Toast.makeText(
                        applicationContext,
                        "Ausgeloggt", Toast.LENGTH_SHORT
                    ).show()

                    getSharedPreferences("login_status", Context.MODE_PRIVATE)
                        .edit()
                        .putBoolean("keep_logged_in", false)
                        .putString("user_email", "")
                        .putString("user_password", "")
                        .apply()

                    val intent = Intent(this, LoginActivityView::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    finish()
                }
            }
            true
        }

        val extras = intent.getStringExtra("intendedFragment")
        if (extras != null) {
            if (extras.equals("dashboard")) {
                supportFragmentManager.commit {
                    replace<DashboardFragment>(R.id.container)
                }
            } else if (extras.equals("dashboardWithLoading")) {
                Thread.sleep(1000)
                supportFragmentManager.commit {
                    replace<DashboardFragment>(R.id.container)
                }
            } else if (extras.equals("training")) {
                supportFragmentManager.commit {
                    val bundle = Bundle().apply {
                        putInt("trainingId", intent.getIntExtra("trainingId", 0))
                    }
                    val trainingFragment = TrainingFragment().apply {
                        arguments = bundle
                    }
                    replace(R.id.container, trainingFragment)
                }
            } else if (extras.equals("trainingResults")) {
                supportFragmentManager.commit {
                    val trainingFragment = TrainingResultsFragment().apply {
                        arguments = intent.getBundleExtra("trainingResultData")
                    }
                    replace(R.id.container, trainingFragment)
                }
            } else if (extras.equals("catalogue")) {
                catFrag = CatalogueFragment()
                val args = Bundle()
                args.putInt("TRAINING_ID", intent.getIntExtra("TRAINING_ID", 0))
                catFrag.arguments = args
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.replace(R.id.container, catFrag)
                transaction.commit()
            } else if (extras.equals("catalogueQuestion")) {
                catQuestionFrag = CatalogueQuestionViewFragment()
                val args = Bundle()
                args.putInt("TRAINING_ID", intent.getIntExtra("TRAINING_ID", 0))
                args.putInt("QUESTION_NUM", intent.getIntExtra("QUESTION_NUM", 0))
                catQuestionFrag.arguments = args
                val manager = supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.replace(R.id.container, catQuestionFrag)
                transaction.commit()
            }
        }
    }

    /**
     * Called when the back button is pressed
     */
    override fun onBackPressed() {
        val currentFragment = supportFragmentManager.findFragmentById(R.id.container)
        if (currentFragment != null && currentFragment is TrainingFragment) {
            (currentFragment).onBackPressed()
        }
        if (currentFragment != null && currentFragment is CatalogueFragment) {
            (currentFragment).onBackPressed()
        }
        if (currentFragment != null && currentFragment is AchievementsFragment) {
            (currentFragment).onBackPressed()
        }
    }
}


