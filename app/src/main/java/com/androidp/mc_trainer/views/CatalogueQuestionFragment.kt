package com.androidp.mc_trainer.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.androidp.mc_trainer.R
import com.androidp.mc_trainer.models.entities.Question

class CatalogueQuestionFragment(val question: Question) : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // hide actionbar
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()

        val view = inflater.inflate(R.layout.fragment_catalogue_question, container, false)
        val trainingQuestion = view.findViewById<TextView>(R.id.training_question)

        trainingQuestion.text = question.question

        if (trainingQuestion.text.count() > 35) {
            trainingQuestion.textSize = 15.0f
        } else {
            trainingQuestion.textSize = 32.0f
        }

        val answerData = arrayOf(
            view.findViewById<Button>(R.id.answer_1) to question.answer1 to question.correct1,
            view.findViewById<Button>(R.id.answer_2) to question.answer2 to question.correct2,
            view.findViewById<Button>(R.id.answer_3) to question.answer3 to question.correct3,
            view.findViewById<Button>(R.id.answer_4) to question.answer4 to question.correct4,
            view.findViewById<Button>(R.id.answer_5) to question.answer5 to question.correct5,
            view.findViewById<Button>(R.id.answer_6) to question.answer6 to question.correct6,
        )

        answerData.forEach {
            if (it.first.second?.isNotBlank() == true) {
                val background = when (it.second) {
                    true -> R.drawable.button_answer_correct
                    else -> R.drawable.button_answer_wrong
                }

                val button = it.first.first

                button.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                button.setBackgroundResource(background)
                button.text = it.first.second ?: ""
                if (button.text.count() > 10) {
                    button.textSize = 10.0f
                } else {
                    button.textSize = 15.0f
                }
            } else {
                it.first.first.visibility = View.INVISIBLE
            }
        }

        return view
    }
}