package com.androidp.mc_trainer.views.utils

import android.content.SharedPreferences
import com.androidp.mc_trainer.models.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Represents an achievement.
 *
 * An achievement consists of a code and an abstract method, which checks, after every training round, if the achievement was achieved.
 */
enum class AchievementsEnum(val code: String) {
    NEWBIE("newbie") {
        override fun getTitle(): String = "Neuling"
        override fun getDescription(): String = "Beende dein erstes Training"

        /*
         * User answers his first question.
         */
        override fun isAchieved(
            options: SharedPreferences,
            userId: String
        ): Boolean {
            return options.getBoolean("finishedTraining", false)
        }
    },

    VETERAN("veteran") {
        override fun getTitle(): String = "Veteran"
        override fun getDescription(): String = "Spiele 10 Trainings"

        /*
         * User finishes 10 trainings
         */
        override fun isAchieved(
            options: SharedPreferences,
            userId: String
        ): Boolean {
            return options.getInt("nrOfPlayedTrainings", 0) != 10
        }
    },

    CLEVER("clever") {
        override fun getTitle(): String = "Clever"
        override fun getDescription(): String = "Beantworte 10 Fragen hinter-\neinander richtig"

        /*
         * User answers 10 questions correctly in a row
         */
        override fun isAchieved(
            options: SharedPreferences,
            userId: String
        ): Boolean {
            return options.getInt("numMaxCorrectlyAnsweredInSuccession", 0) >= 10
        }
    },

    NERD("nerd") {
        override fun getTitle(): String = "Nerd"
        override fun getDescription(): String = "Spiele ein Training ohne Fehler durch"

        /*
         * user finishes a training without errors
         */
        override fun isAchieved(
            options: SharedPreferences,
            userId: String
        ): Boolean {
            return options.getInt("numIncorrectlyAnswered", -1) == 0
        }
    },

    UNLUCKY("unlucky") {
        override fun getTitle(): String = "Pechvogel"
        override fun getDescription(): String = "In einem Training keine Frage richtig beantwortet"

        /*
         * user finishes a training without answering a single question correctly
         */
        override fun isAchieved(
            options: SharedPreferences,
            userId: String
        ): Boolean {
            return options.getInt("numCorrectlyAnswered", -1) == 0
        }
    };


    abstract fun getTitle(): String

    abstract fun getDescription(): String

    /**
     * Checks if the achievement was achieved
     *
     * @param options Contains the achievement data
     * @param userId The User ID
     * @return False if the achievement was not achieved, true if it was.
     */
    abstract fun isAchieved(
        options: SharedPreferences,
        userId: String
    ): Boolean

    companion object {
        /**
         * Checks all achievements
         *
         * @param options Contains the achievement data
         * @param currentUser The User ID
         */
        fun checkAchievements(options: SharedPreferences, currentUser: String) {
            val app = Application()
            val repo = app.achievementsRepository

            values().forEach {
                if (it.isAchieved(options, currentUser)) {
                    CoroutineScope(Dispatchers.IO).launch {
                        repo.achieved(currentUser, it.code)
                    }
                }
            }
        }
    }
}