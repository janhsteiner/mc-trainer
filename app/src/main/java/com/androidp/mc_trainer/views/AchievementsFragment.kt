package com.androidp.mc_trainer.views

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidp.mc_trainer.R
import com.androidp.mc_trainer.models.Application
import com.androidp.mc_trainer.models.entities.Achievement
import com.androidp.mc_trainer.viewmodels.AchievementsViewModel
import com.androidp.mc_trainer.viewmodels.AchievementsViewModelFactory
import com.androidp.mc_trainer.views.utils.AchievementsListAdapter
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Shows the achievement page
 */
class AchievementsFragment(): Fragment() {
    /**
     * Called when the view is being destroyed
     */
    override fun onDestroyView() {
        (activity as MainActivityView).navView.menu.findItem(R.id.item_achievements).setVisible(true)
        super.onDestroyView()
    }

    /**
     * Called when the fragment is created
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return The new view
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // show actionbar
        (requireActivity() as AppCompatActivity).supportActionBar?.show()
        (activity as MainActivityView).navView.menu.findItem(R.id.item_achievements).setVisible(false)
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_achievements, container, false)
    }

    /**
     * Called when the view is created
     *
     * @param view                  The view
     * @param savedInstanceState    The saved instance state
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mainActivity = activity as MainActivityView
        val adapter = AchievementsListAdapter(requireContext())

        mainActivity.title = "Achievements"

        val viewmodel: AchievementsViewModel by viewModels {
            AchievementsViewModelFactory(
                (activity?.application as Application).achievementsRepository
            )
        }

        val gridview = view.findViewById<RecyclerView>(R.id.achievements_grid)
        val layoutManager = GridLayoutManager(
            activity,
            2,
            RecyclerView.VERTICAL,
            false
        )
        gridview.layoutManager = layoutManager

        CoroutineScope(Dispatchers.IO).launch {
            adapter.list = viewmodel.getAllAchievements(mainActivity.user)
                    as MutableList<Achievement>
        }.invokeOnCompletion {
            gridview.adapter = adapter
        }
    }

    /**
     * Called when the back button was pressed
     */
    fun onBackPressed() {
        val i = Intent(context, MainActivityView::class.java)
        i.putExtra("intendedFragment", "dashboard")
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        activity?.applicationContext?.startActivity(i)
    }
}