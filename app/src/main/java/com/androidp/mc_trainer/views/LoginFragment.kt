package com.androidp.mc_trainer.views

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import androidx.fragment.app.viewModels
import com.androidp.mc_trainer.R
import com.androidp.mc_trainer.viewmodels.LoginViewModel
import com.androidp.mc_trainer.viewmodels.LoginViewModelFactory

/**
 * Displays the login screen
 */
class LoginFragment : Fragment() {
    /**
     * Called when the fragment is created
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    /**
     * Called when the view is created
     *
     * @param view
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewModel: LoginViewModel by viewModels {
            LoginViewModelFactory()
        }

        // todo: move to loading screen when it is available
        val sp = requireContext().getSharedPreferences("login_status", Context.MODE_PRIVATE)
        if (sp.getBoolean("keep_logged_in", false)) {
            val email = sp.getString("user_email", "")
            val password = sp.getString("user_password", "")

            if (!email.isNullOrBlank() && !password.isNullOrBlank())
                viewModel.loginUser(email, password)
        }

        val passwordText = view.findViewById<EditText>(R.id.password)
        val emailText = view.findViewById<EditText>(R.id.email).apply {
            // fixme:
            // cache the content of the email text field and restore it e.g. on device rotation
            // ich denke es ist kaputt gegangen weil wir jetzt das viewmodel vom fragment benutzen
            // beim drehen wird aber auch die activity neu gestarted und das viewmodel hier neu erstllt wird
            this.setText(viewModel.emailTextContent)

            addTextChangedListener {
                viewModel.emailTextContent = this.text.toString()
            }
        }

        val keepLoggedIn = view.findViewById<CheckBox>(R.id.keep_user_logged_in)

        viewModel.loginStatus.observe(viewLifecycleOwner) {
            val status = viewModel.loginStatus.value
            val notificationContainer =
                view.findViewById<ConstraintLayout>(R.id.notificationContainer)
            val notificationText = view.findViewById<TextView>(R.id.notificationText)
            val notificationClose = view.findViewById<ImageButton>(R.id.closeNotification)

            val notificationOpenAnimation = AnimationUtils.loadAnimation(
                context,
                R.anim.inflate_from_top
            )
            val notificationCloseAnimation = AnimationUtils.loadAnimation(
                context,
                R.anim.deflate_to_top
            )

            notificationContainer.animation = notificationOpenAnimation
            notificationContainer.animation.fillAfter = true
            notificationClose.setOnClickListener {
                notificationContainer.startAnimation(notificationCloseAnimation)
            }

            // status is a livedata obj in the viewmodel and determines the status of the
            // login process
            when (status) {
                LoginStatus.SUCCESS -> {
                    // success
                    // save login credentials if the user wants to be kept logged in
                    if (keepLoggedIn.isChecked) {
                        requireContext().getSharedPreferences("login_status", Context.MODE_PRIVATE)
                            .edit()
                            .putBoolean("keep_logged_in", true)
                            .putString("user_email", emailText.text.toString())
                            .putString("user_password", passwordText.text.toString())
                            .apply()
                    }
                    parentFragmentManager.commit {
                        addToBackStack(null)
                        replace<LaunchingFragment>(R.id.container)
                    }
                }
                LoginStatus.EMAIL_EMPTY_OR_INCORRECT -> {
                    // email is empty
                    notificationText.setText(R.string.notification_email_empty)
                    notificationContainer.animation.start()
                }
                LoginStatus.PASSWORD_EMPTY -> {
                    // password is empty
                    notificationText.setText(R.string.notification_password_empty)
                    notificationContainer.animation.start()
                }
                LoginStatus.ERROR -> {
                    // error
                    notificationText.setText(R.string.notification_error)
                    notificationContainer.animation.start()
                }
                LoginStatus.NOT_REGISTERED -> {
                    // user not registered
                    notificationText.setText(R.string.notification_not_registered)
                    notificationContainer.animation.start()
                }
                LoginStatus.FAILED -> {
                    // email or password incorrect
                    notificationText.setText(R.string.notification_email_or_password_incorrect)
                    notificationContainer.animation.start()
                }
                LoginStatus.INIT -> {
                    notificationText.setText(R.string.notification_unknown_error)
                    notificationContainer.animation.start()
                }
            }
        }

        view.findViewById<Button>(R.id.login).apply {
            setOnClickListener {
                viewModel.loginUser(emailText.text.toString(), passwordText.text.toString())
            }
        }

        view.findViewById<Button>(R.id.sign_up).setOnClickListener {
            /* Go to the registration page */
            parentFragmentManager.commit {
                addToBackStack(null)
                replace<RegisterFragment>(R.id.container)
            }
        }
    }
}