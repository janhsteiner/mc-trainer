package com.androidp.mc_trainer.views

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.androidp.mc_trainer.R
import com.androidp.mc_trainer.models.Application
import com.androidp.mc_trainer.models.entities.Question
import com.androidp.mc_trainer.viewmodels.CatalogueQuestionsViewModel
import com.androidp.mc_trainer.viewmodels.CatalogueQuestionsViewModelFactory
import com.androidp.mc_trainer.views.CatalogueQuestionViewFragment.CatalogueQuestionPagerAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Displays the catalogue questions with a scrolling feature
 *
 * @see CatalogueQuestionPagerAdapter
 */
class CatalogueQuestionViewFragment : Fragment() {
    private var trainingId = 0

    private lateinit var pager: ViewPager2

    /**
     * Called when the fragment is created
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // hide actionbar
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_catalogue_question, container, false)
    }

    /**
     * Called when the view is created
     *
     * @param view
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val catalogueQuestionsViewModel: CatalogueQuestionsViewModel by viewModels {
            CatalogueQuestionsViewModelFactory(
                (activity?.application as Application).questionRepository,
                (activity?.application as Application).trainingRepository,
                trainingId
            )
        }

        trainingId = arguments?.getInt("TRAINING_ID", 1)!!
        var questionNum = arguments?.getInt("QUESTION_NUM", 0) ?: 0

        pager = view.findViewById(R.id.pager)

        var currentQuestionId = 0

        catalogueQuestionsViewModel.getQuestionlist().observe(viewLifecycleOwner) {
            val adapter = CatalogueQuestionPagerAdapter(it, requireActivity())
            pager.adapter = adapter

            val questionNumberText = view.findViewById<TextView>(R.id.question_num)

            pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    val questionNumberTextContent = "Frage ${pager.currentItem + 1} von " +
                            "${adapter.itemCount}"
                    questionNumberText.text = questionNumberTextContent
                    questionNum = pager.currentItem
                    currentQuestionId = it[pager.currentItem].id
                }
            })

            currentQuestionId = run {
                pager.setCurrentItem(questionNum, false)
                it[questionNum].id
            }
        }

        view.findViewById<ImageButton>(R.id.back_button).setOnClickListener {
            requireActivity().finish()
        }

        view.findViewById<ImageButton>(R.id.reset_score).setOnClickListener {
            AlertDialog.Builder(context)
                .setTitle(R.string.catalogue_reset_question_title)
                .setMessage(R.string.catalogue_reset_question_msg)
                .setPositiveButton(R.string.yes) { _, _ ->
                    // Reset score
                    lifecycleScope.launch(Dispatchers.IO) {
                        catalogueQuestionsViewModel.resetScoreForQuestions(currentQuestionId)
                    }
                }
                .setNegativeButton(R.string.no, null)
                .setIcon(R.drawable.ic_baseline_warning_24)
                .show()
        }
    }

    /**
     * Acts as an adapter for the ViewPager
     */
    private inner class CatalogueQuestionPagerAdapter(
        private val questions: List<Question>,
        private val activity: FragmentActivity
    ) : FragmentStateAdapter(activity) {
        /**
         * Returns the item count
         *
         * @return item count
         */
        override fun getItemCount() = questions.count()

        /**
         * Creates the fragment for the corresponding position
         *
         * @param position the position
         * @return the catalogue question fragment
         * @see CatalogueQuestionFragment
         */
        override fun createFragment(position: Int) = CatalogueQuestionFragment(questions[position])
    }
}