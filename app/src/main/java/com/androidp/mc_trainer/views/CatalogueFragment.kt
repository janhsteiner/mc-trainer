package com.androidp.mc_trainer.views

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.androidp.mc_trainer.R
import com.androidp.mc_trainer.models.Application
import com.androidp.mc_trainer.models.entities.Question
import com.androidp.mc_trainer.viewmodels.CatalogueQuestionsViewModel
import com.androidp.mc_trainer.viewmodels.CatalogueQuestionsViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Displays a question catalogue
 */
class CatalogueFragment : Fragment() {
    private var trainingId = 0
    private lateinit var questionsAdapter: ArrayAdapter<Question>

    /**
     * Called when the fragment is created
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // hide actionbar
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_catalogue, container, false)
    }

    /**
     * Called when the view is created
     *
     * @param view
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val catalogueQuestionsViewModel: CatalogueQuestionsViewModel by viewModels {
            CatalogueQuestionsViewModelFactory(
                (activity?.application as Application).questionRepository,
                (activity?.application as Application).trainingRepository,
                trainingId
            )
        }

        trainingId = arguments?.getInt("TRAINING_ID", 0)!!

        val catalogueTitle = view.findViewById<TextView>(R.id.catalogue_title)
        val questionsList = view.findViewById<ListView>(R.id.questions_list)

        questionsAdapter = activity?.let { QuestionsAdapter(it) }!!
        questionsList.adapter = questionsAdapter

        catalogueQuestionsViewModel.getTrainingName().observe(viewLifecycleOwner) {
            catalogueTitle.text = it
        }

        catalogueQuestionsViewModel.getQuestionlist().observe(viewLifecycleOwner) {
            questionsAdapter.clear()
            questionsAdapter.addAll(it)

            view.findViewById<ImageButton>(R.id.reset_score).setOnClickListener { _ ->
                AlertDialog.Builder(context)
                    .setTitle(R.string.catalogue_reset_title)
                    .setMessage(R.string.catalogue_reset_title)
                    .setPositiveButton("Ja") { _, _ ->
                        // Reset scores
                        lifecycleScope.launch(Dispatchers.IO) {
                            it.forEach { q ->
                                catalogueQuestionsViewModel
                                    .resetScoreForQuestions(q.id)
                            }
                        }
                    }
                    .setNegativeButton(R.string.no, null)
                    .setIcon(R.drawable.ic_baseline_warning_24)
                    .show()
            }
        }

        questionsList.onItemClickListener =
            AdapterView.OnItemClickListener { _, _, position, _ ->
                val i = Intent(context, MainActivityView::class.java)
                i.putExtra("intendedFragment", "catalogueQuestion")
                i.putExtra("TRAINING_ID", trainingId)
                i.putExtra("QUESTION_NUM", position)
                startActivity(i)
            }

        view.findViewById<ImageButton>(R.id.back_button).setOnClickListener {
            requireActivity().finish()
        }
    }

    /**
     * Adapter for the questions list
     */
    private inner class QuestionsAdapter(context: Context) : ArrayAdapter<Question>(
        context,
        R.layout.catalogue_question_list_item,
        mutableListOf<Question>()
    ) {
        /**
         * Gets the view for a certain question
         *
         * @param position item position
         * @param convertView the view (if already available)
         * @param parent the view group
         * @result the view of the question
         */
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val view = if (convertView == null) {
                val inflater = LayoutInflater.from(context)
                inflater.inflate(R.layout.catalogue_question_list_item, null)
            } else {
                convertView
            }

            val question = getItem(position)!!

            val questionNum = view.findViewById<TextView>(R.id.question_num)
            val questionName = view.findViewById<TextView>(R.id.question_name)
            val questionScore = view.findViewById<TextView>(R.id.question_score)

            val questionNumText = "Frage ${position + 1}"
            questionNum.text = questionNumText
            questionName.text = question.question.take(30)
            val questionScoreText = "Score: ${question.score}"
            questionScore.text = questionScoreText
            return view
        }
    }

    fun onBackPressed() {
        val i = Intent(context, MainActivityView::class.java)
        i.putExtra("intendedFragment", "dashboard")
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        activity?.applicationContext?.startActivity(i)
    }
}