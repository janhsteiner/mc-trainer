package com.androidp.mc_trainer.views

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.androidp.mc_trainer.R
import com.androidp.mc_trainer.models.Application
import com.androidp.mc_trainer.viewmodels.DashboardViewModel
import com.androidp.mc_trainer.viewmodels.DashboardViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Represents the loading screen
 */
class LaunchingFragment : Fragment() {
    /**
     * Called when the fragment is created
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_launching, container, false)
    }

    /**
     * Called when the view is created
     *
     * @param view
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dashboardViewModel: DashboardViewModel by viewModels {
            DashboardViewModelFactory(
                (activity?.application as Application).trainingRepository,
                (activity?.application as Application).questionRepository,
                (activity?.application as Application).firebaseRepository,
                (activity?.application as Application).achievementsRepository,
                (activity?.application as Application).pusRepository,
            )
        }
        CoroutineScope(Dispatchers.IO).launch {
            dashboardViewModel.syncDatabases()
        }.invokeOnCompletion {
            val i = Intent(context, MainActivityView::class.java)
            i.putExtra("intendedFragment", "dashboardWithLoading")
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context?.startActivity(i)
        }
    }
}