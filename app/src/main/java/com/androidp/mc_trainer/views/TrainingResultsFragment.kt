package com.androidp.mc_trainer.views

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.androidp.mc_trainer.R

/**
 * Shows a summary of a training phase at the end
 */
class TrainingResultsFragment : Fragment() {
    /**
     * Called when the fragment is created
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // hide actionbar
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_training_results, container, false)
    }

    /**
     * Called when the view is inflated
     *
     * @param inflater
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /* Grab arguments */
        val trainingId = arguments?.getInt("trainingId") ?: 0
        val numCorrectlyAnswered = arguments?.getInt("numCorrectlyAnswered") ?: 0
        val numIncorrectlyAnswered = arguments?.getInt("numIncorrectlyAnswered") ?: 0

        /* Assign values */
        view.findViewById<TextView>(R.id.result_count_questions).text =
            "${numCorrectlyAnswered + numIncorrectlyAnswered} Frage(n)"
        view.findViewById<TextView>(R.id.result_correctly_answered).text =
            "$numCorrectlyAnswered Frage(n)"
        view.findViewById<TextView>(R.id.result_incorrectly_answered).text =
            "$numIncorrectlyAnswered Frage(n)"

        /* Setup back button */
        view.findViewById<ImageButton>(R.id.back_button).setOnClickListener {
            val i = Intent(context, MainActivityView::class.java)
            i.putExtra("intendedFragment", "dashboard")
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            activity?.applicationContext?.startActivity(i)
        }

        /* Setup repeat training button */
        view.findViewById<Button>(R.id.repeat_training).setOnClickListener {
            val i = Intent(context, MainActivityView::class.java)
            i.putExtra("intendedFragment", "training")
            i.putExtra("trainingId", trainingId)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            activity?.applicationContext?.startActivity(i)
        }

        /* Setup second back button */
        view.findViewById<Button>(R.id.go_back).setOnClickListener {
            val i = Intent(context, MainActivityView::class.java)
            i.putExtra("intendedFragment", "dashboard")
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            activity?.applicationContext?.startActivity(i)
        }
    }
}