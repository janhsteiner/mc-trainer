package com.androidp.mc_trainer.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.androidp.mc_trainer.R
import com.androidp.mc_trainer.models.Application
import com.androidp.mc_trainer.models.entities.Training
import com.androidp.mc_trainer.viewmodels.DashboardViewModel
import com.androidp.mc_trainer.viewmodels.DashboardViewModelFactory
import com.androidp.mc_trainer.views.utils.DashboardListAdapter
import kotlinx.android.synthetic.main.activity_dashboard.*

/**
 * Displays the dashboard
 */
class DashboardFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView

    /**
     * Called when the fragment is being destroyed
     */
    override fun onDestroyView() {
        (activity as MainActivityView).navView.menu.findItem(R.id.item_dashboard).setVisible(true)
        super.onDestroyView()
    }

    /**
     * Called when the fragment is created
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // show appbar
        (requireActivity() as AppCompatActivity).supportActionBar?.show()
        // hide dashboard item
        (activity as MainActivityView).navView.menu.findItem(R.id.item_dashboard).setVisible(false)
        // change title
        (activity as MainActivityView).title = "Wähle ein Training aus"
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    /**
     * Called when the view is created
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navView?.menu?.findItem(R.id.item_dashboard)?.setVisible(false)

        val dashboardViewModel: DashboardViewModel by viewModels {
            DashboardViewModelFactory(
                (activity?.application as Application).trainingRepository,
                (activity?.application as Application).questionRepository,
                (activity?.application as Application).firebaseRepository,
                (activity?.application as Application).achievementsRepository,
                (activity?.application as Application).pusRepository,
            )
        }

        // set toolbar
        // setSupportActionBar(findViewById(R.id.toolbar))
        // supportActionBar?.title = resources.getString(R.string.dashboard_title)

        // set recyclerview and adapter
        recyclerView = view.findViewById(R.id.dashboard_recyclerview)
        val adapter = activity?.let { DashboardListAdapter(it) }

        // library for gridlayout may cause the app to not compile
        val layoutManager = GridLayoutManager(
            activity,
            2,
            RecyclerView.VERTICAL,
            false
        )

        dashboardViewModel.trainingList.observe(viewLifecycleOwner) { trainings ->
            // null check because we can't cast a null object to a MutableList
            if (trainings != null && trainings.isNotEmpty()) {
                if (adapter != null) {
                    adapter.list = trainings as MutableList<Training>
                }
                recyclerView.adapter = adapter
            }
        }

        dashboardViewModel.uploadData()
        recyclerView.layoutManager = layoutManager
    }
}
