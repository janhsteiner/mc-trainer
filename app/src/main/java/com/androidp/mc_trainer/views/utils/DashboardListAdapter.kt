package com.androidp.mc_trainer.views.utils

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.graphics.scale
import androidx.recyclerview.widget.RecyclerView
import com.androidp.mc_trainer.R
import com.androidp.mc_trainer.models.entities.Training
import com.androidp.mc_trainer.views.MainActivityView

/**
 * Acts as list adapter for training modules displayed on the dashboard
 */
class DashboardListAdapter(private val context: Context) :
    RecyclerView.Adapter<DashboardListAdapter.ItemViewHolder>() {

    // this list represents all elements
    // updated by livedata observer
    var list: MutableList<Training> = mutableListOf()

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var trainingTitle = itemView.findViewById<TextView>(R.id.dashboard_element_title)
        var trainingThumbnail =
            itemView.findViewById<ImageView>(R.id.dashboard_element_thumbnail)
        var trainingCatalogue = itemView.findViewById<ImageView>(R.id.dashboard_element_catalogue)
        var trainingStart = itemView.findViewById<ImageView>(R.id.dashboard_element_start)
        var trainingButtons = itemView.findViewById<ConstraintLayout>(R.id.linearLayout)
    }

    /**
     * This function provides the number of elements which the recyclerView renders.
     *
     * @return the size of [list]
     */
    override fun getItemCount(): Int {
        return list.size
    }

    /**
     * This function is called when the RecyclerView needs a view holder to represent an item.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            DashboardListAdapter.ItemViewHolder {

        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.dashboard_element, parent, false)
        return ItemViewHolder(v)
    }

    /**
     * This function "assembles" every element. The values and the view of every
     * dashboard element are set here.
     */
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        var overlayTriggered = false
        val item = list[position]
        holder.trainingTitle.text = item.moduleName

        val filepath =
            Environment.getDataDirectory().absolutePath +
                    "/data/com.androidp.mc_trainer/files/" +
                    item.imageName

        var bitmap: Bitmap =
            try {
                BitmapFactory.decodeFile(filepath)
            } catch (e: NullPointerException) {
                val p = context.resources.openRawResource(R.raw.default_thumbnail)
                BitmapFactory.decodeStream(p)
            }

        // bitmap only accepts ints while the best-practive for layouts is dp
        // conversion from int to dp taken from
        // https://gist.github.com/titoaesj/ccd7ddc3c40350217f2bcae248d2ffc3
        val height = (130 * Resources.getSystem().displayMetrics.density + 0.5f).toInt()
        val width = (200 * Resources.getSystem().displayMetrics.density + 0.5f).toInt()

        bitmap = bitmap.scale(width, height, false)
        holder.trainingThumbnail.setImageBitmap(bitmap)

        holder.trainingThumbnail.setOnClickListener {
            overlayTriggered =
                if (!overlayTriggered) {
                    holder.trainingTitle
                        .animate()
                        .translationY(-height.toFloat())
                        .setDuration(200)
                        .start()

                    // animation only renders the moved pixels, not the element itself!
                    // in order to make the elements clickable we have to move the container up and down
                    holder.trainingButtons
                        .animate()
                        .translationYBy(-height.toFloat())
                        .setDuration(200)
                        .start()

                    true
                } else {
                    holder.trainingTitle
                        .animate()
                        .translationY(0F)
                        .setDuration(200)
                        .start()

                    holder.trainingButtons
                        .animate()
                        .translationYBy(height.toFloat())
                        .setDuration(200)
                        .start()

                    holder.trainingCatalogue.bringToFront()
                    holder.trainingStart.bringToFront()
                    false
                }
        }

        holder.trainingCatalogue.setOnClickListener {
            val i = Intent(context, MainActivityView::class.java)
            i.putExtra("intendedFragment", "catalogue")
            i.putExtra("TRAINING_ID", item.trainingId)
            context.startActivity(i)
        }

        holder.trainingStart.setOnClickListener {
            val i = Intent(context, MainActivityView::class.java)
            i.putExtra("intendedFragment", "training")
            i.putExtra("trainingId", item.trainingId)
            context.startActivity(i)
        }
    }
}