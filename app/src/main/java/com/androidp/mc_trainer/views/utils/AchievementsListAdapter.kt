package com.androidp.mc_trainer.views.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.androidp.mc_trainer.R
import com.androidp.mc_trainer.models.entities.Achievement

/**
 * Acts as adapter for the achievements list
 */
class AchievementsListAdapter(private val context: Context) :
    RecyclerView.Adapter<AchievementsListAdapter.ItemViewHolder>() {

    // this list represents all achievements
    var achievementList: Array<AchievementsEnum> = AchievementsEnum.values()
    var list = mutableListOf<Achievement>()

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var achievementTitle = itemView.findViewById<TextView>(R.id.achievement_title)
        var achievementDescription = itemView.findViewById<TextView>(R.id.achievement_description)
        var achievementAchieved = itemView.findViewById<ImageView>(R.id.achievement_achieved)
    }

    /**
     * This function provides the number of elements which the recyclerView renders.
     *
     * @return the size of [achievementList]
     */
    override fun getItemCount(): Int {
        return achievementList.size
    }

    /**
     * This function is called when the RecyclerView needs a view holder to represent an item.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            AchievementsListAdapter.ItemViewHolder {

        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.achievements_element, parent, false)
        return ItemViewHolder(v)
    }

    /**
     * This function "assembles" every element. The values for every achievement element are set
     * here.
     */
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.achievementTitle.text = achievementList[position].getTitle()
        holder.achievementDescription.text = achievementList[position].getDescription()

        val wasAchieved = list.find { it.code == achievementList[position].code }?.achieved ?: false

        if (wasAchieved)
            holder.achievementAchieved.visibility = View.VISIBLE
    }
}