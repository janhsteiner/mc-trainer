package com.androidp.mc_trainer.viewmodels

import androidx.lifecycle.*
import com.androidp.mc_trainer.models.entities.Question
import com.androidp.mc_trainer.models.entities.Training
import com.androidp.mc_trainer.models.repositories.PublicUserScoresRepository
import com.androidp.mc_trainer.models.repositories.QuestionRepository
import com.androidp.mc_trainer.models.repositories.TrainingRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * View model for the training fragment
 */
class TrainingViewModel(
    private val questionsRepo: QuestionRepository,
    private val trainingsRepo: TrainingRepository,
    private val pusRepo: PublicUserScoresRepository,
    private val trainingId: Int
) : ViewModel() {
    private val questionList: LiveData<List<Question>> =
        questionsRepo.getQuestionsForTraining(trainingId)

    private val training: LiveData<Training> = trainingsRepo.getTraining(trainingId)

    fun getQuestionlist(): LiveData<List<Question>> =
        questionList

    private val seed = System.currentTimeMillis()

    /**
     * Gets a set of randomized questions for this training phase
     *
     * @return a live data list of questions
     */
    fun getRandomizedQuestions(): LiveData<List<Question>> =
        // todo: we should probably remove this entry from the list
        Transformations.map(questionList) {
            // Sort by score ascending, then take the first 15 questions and shuffle those
            it.sortedBy { question -> question.score }.take(15).shuffled()
        }

    /**
     * Increases the score for the given question IDs
     *
     * @param questions The question IDs
     */
    fun increaseScoreForQuestionIds(questions: List<Int>) {
        questions.forEach { q ->
            CoroutineScope(Dispatchers.IO).launch {
                val question = questionsRepo.getQuestion(q)
                val user = questionsRepo.getUserForQuestion(question.trainingId, question.questionId)
                when (user == "public") {
                    true -> pusRepo.incrementScore(question.id)
                    false -> questionsRepo.increaseScoreForQuestion(q)
                }
            }
        }
    }

    fun resetScoreForQuestionIds(questions: List<Int>) {
        questions.forEach { q ->
            CoroutineScope(Dispatchers.IO).launch {
                val question = questionsRepo.getQuestion(q)
                val user = questionsRepo.getUserForQuestion(question.trainingId, question.id)
                when (user == "public") {
                    true -> pusRepo.resetScore(question.id)
                    else -> questionsRepo.resetScoreForQuestions(q)
                }
            }
        }
    }

    /**
     * Returns the training's name
     *
     * @return The training name
     */
    fun getTrainingName(): LiveData<String> =
        training.map { it.moduleName }
}

/**
 * ViewModel factory
 * @see RegisterViewModel
 */
class TrainingViewModelFactory(
    private val questionsRepo: QuestionRepository,
    private val trainingsRepo: TrainingRepository,
    private val pusRepo: PublicUserScoresRepository,
    private val trainingId: Int
) : ViewModelProvider.Factory {

    /**
     * Creates the view model of the specific class
     *
     * @param modelClass The model class
     * @return An instance of the view model
     */
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(TrainingViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return TrainingViewModel(
                questionsRepo,
                trainingsRepo,
                pusRepo,
                trainingId
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}