package com.androidp.mc_trainer.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.androidp.mc_trainer.models.repositories.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * View model for the Dashboard fragment
 */
class DashboardViewModel(
    private val trainingRepository: TrainingRepository,
    private val questionRepository: QuestionRepository,
    private val firebaseRepository: FirebaseRepository,
    private val achievementsRepository: AchievementsRepository,
    private val pusRepository: PublicUserScoresRepository
) : ViewModel() {

    private val user = FirebaseAuth.getInstance().currentUser?.uid

    // MutableLivedata should only be visible to our specific ViewModel
    var trainingList = trainingRepository.getAllTrainingsSynced(user!!)

    /**
     * Asynchronously downloads the trainings and questions for the currently logged in user
     */
    fun syncDatabases() {
        val job = CoroutineScope(Dispatchers.IO).launch {
            firebaseRepository.syncDatabasesForUser(
                user!!,
                trainingRepository,
                questionRepository,
                achievementsRepository,
                pusRepository
            )
        }

        // wait for downloads
        runBlocking {
            job.join()
        }
    }

    fun uploadData() {
        firebaseRepository.uploadDataToFirebase(
            user!!,
            questionRepository,
            achievementsRepository,
            pusRepository
        )
    }
}

/**
 * ViewModel factory
 * @see DashboardViewModel
 */
class DashboardViewModelFactory(
    private val trainingRepository: TrainingRepository,
    private val questionRepository: QuestionRepository,
    private val firebaseRepository: FirebaseRepository,
    private val achievementsRepository: AchievementsRepository,
    private val pusRepository: PublicUserScoresRepository
) : ViewModelProvider.Factory {
    /**
     * Creates the view model of the specific class
     *
     * @param modelClass The model class
     * @return An instance of the view model
     */
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DashboardViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DashboardViewModel(
                trainingRepository,
                questionRepository,
                firebaseRepository,
                achievementsRepository,
                pusRepository
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}