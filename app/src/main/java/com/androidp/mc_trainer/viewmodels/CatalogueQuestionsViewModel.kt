package com.androidp.mc_trainer.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.map
import com.androidp.mc_trainer.models.entities.Question
import com.androidp.mc_trainer.models.entities.Training
import com.androidp.mc_trainer.models.repositories.QuestionRepository
import com.androidp.mc_trainer.models.repositories.TrainingRepository

/**
 * ViewModel for the catalogue sections
 */
class CatalogueQuestionsViewModel(
    private val questionsRepo: QuestionRepository,
    private val trainingsRepo: TrainingRepository,
    private val trainingId: Int
) : ViewModel() {
    private val questionList: LiveData<List<Question>> =
        questionsRepo.getQuestionsForCatalogue(trainingId)

    private val training: LiveData<Training> = trainingsRepo.getTraining(trainingId)

    /**
     * Gets the list of questions for this training
     * @param List of questions
     */
    fun getQuestionlist(): LiveData<List<Question>> =
        questionList

    /**
     * Resets the score for the given question IDs
     * @param questionIds The question IDs
     */
    fun resetScoreForQuestions(questionIds: Int)
        = questionsRepo.resetScoreForQuestions(questionIds)

    /**
     * Gets the training's name
     * @return The training name
     */
    fun getTrainingName(): LiveData<String> =
        training.map { it.moduleName }
}

/**
 * ViewModel factory
 * @see CatalogueQuestionsViewModel
 */
class CatalogueQuestionsViewModelFactory(
    private val questionsRepo: QuestionRepository,
    private val trainingsRepo: TrainingRepository,
    private val trainingId: Int
) :
    ViewModelProvider.Factory {
    /**
     * Creates the view model of the specific class
     *
     * @param modelClass The model class
     * @return An instance of the view model
     */
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CatalogueQuestionsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CatalogueQuestionsViewModel(questionsRepo, trainingsRepo, trainingId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}