package com.androidp.mc_trainer.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.androidp.mc_trainer.models.entities.Achievement
import com.androidp.mc_trainer.models.repositories.AchievementsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Viewmodel for the achievements page
 */
class AchievementsViewModel(
    private val repo: AchievementsRepository
) : ViewModel() {
    var achievementsList = mutableListOf<Achievement>()

    /**
     * Gets all achievements of the user
     *
     * @param userUid The User ID
     * @return The list of achievements
     */
    suspend fun getAllAchievements(userUid: String): List<Achievement> {
        CoroutineScope(Dispatchers.IO).launch {
            achievementsList = repo.getAllAchievements(userUid) as MutableList<Achievement>
        }.join()

        return achievementsList
    }
}

/**
 * ViewModel factory
 * @see AchievementsViewModel
 */
class AchievementsViewModelFactory(
    private val repo: AchievementsRepository
) : ViewModelProvider.Factory {
    /**
     * Creates the view model of the specific class
     *
     * @param modelClass The model class
     * @return An instance of the view model
     */
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AchievementsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return AchievementsViewModel(
                repo
            ) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}