package com.androidp.mc_trainer.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.androidp.mc_trainer.views.RegisterStatus
import com.google.firebase.auth.FirebaseAuth

/**
 * View model for the registration page
 */
class RegisterViewModel : ViewModel() {
    private lateinit var auth: FirebaseAuth
    var registerStatus = MutableLiveData<RegisterStatus>()
    var email: String = ""
    var password: String = ""
    var passwordConfirmation: String = ""

    /**
     * Performs the user registration
     *
     * @param email The user email
     * @param password The user password
     */
    fun register(email: String, password: String) {
        auth = FirebaseAuth.getInstance()
        auth.createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                registerStatus.value = RegisterStatus.SUCCESS
            }
            .addOnFailureListener {
                // if the task failed, check if the user is already registered
                val authMethods = auth.fetchSignInMethodsForEmail(email)

                while (!authMethods.isComplete) {
                    if (authMethods.isCanceled) {
                        registerStatus.value = RegisterStatus.FAILURE
                        return@addOnFailureListener
                    }
                    Thread.sleep(100)
                }

                // user is already registered
                if (authMethods.result?.signInMethods?.size!! > 0) {
                    registerStatus.value = RegisterStatus.EXISTS
                }
            }
    }
}

/**
 * ViewModel factory
 * @see RegisterViewModel
 */
class RegisterViewModelFactory :
    ViewModelProvider.Factory {
    /**
     * Creates the view model of the specific class
     *
     * @param modelClass The model class
     * @return An instance of the view model
     */
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RegisterViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return RegisterViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}