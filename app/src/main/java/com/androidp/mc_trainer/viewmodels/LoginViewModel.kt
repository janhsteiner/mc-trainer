package com.androidp.mc_trainer.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.androidp.mc_trainer.views.LoginStatus
import com.google.firebase.auth.FirebaseAuth
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * View model for the login fragment
 */
class LoginViewModel : ViewModel() {
    private lateinit var auth: FirebaseAuth
    var loginStatus = MutableLiveData<LoginStatus>()
    var emailTextContent = ""

    /**
     * Performs the log in. Changes loginStatus based on result.
     *
     * @param email The user email
     * @param password The user password
     */
    fun loginUser(email: String?, password: String?) {
        // email is empty or not valid
        if (!isEmailValid(email)) {
            loginStatus.value = LoginStatus.EMAIL_EMPTY_OR_INCORRECT
            return
        } else if (password == null || password.isEmpty()) {
            loginStatus.value = LoginStatus.PASSWORD_EMPTY
            return
        }

        auth = FirebaseAuth.getInstance()

        auth.signInWithEmailAndPassword(email!!, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    loginStatus.value = LoginStatus.SUCCESS
                } else {
                    // task was not successful
                    auth.fetchSignInMethodsForEmail(email)
                        .addOnSuccessListener { res ->
                            // is user even registered?
                            if (res.signInMethods == null ||
                                res.signInMethods!!.isEmpty()
                            ) {
                                loginStatus.value = LoginStatus.NOT_REGISTERED
                            } else {
                                // user is registered, but password or email is not correct
                                loginStatus.value = LoginStatus.FAILED
                            }
                        }
                        .addOnFailureListener {
                            loginStatus.value = LoginStatus.ERROR
                        }
                }
            }
            .addOnFailureListener {
                loginStatus.value = LoginStatus.ERROR
            }
    }

    /**
     * Checks if the provided string is a valid email address.
     * The email is considered valid if
     *
     *     1. it is not empty or consists solely of whitespaces
     *     2. if it follows the email pattern (e.g. something@example.test)
     *
     * @param email the email string, which should be checked
     * @return true if it is valid, else false
     */
    private fun isEmailValid(email: String?): Boolean {
        if (email == null || email.isBlank())
            return false

        val p: Pattern = Pattern.compile("^(\\w+.)*\\w+@(\\w+)(.\\w+)+\$")
        val m: Matcher = p.matcher(email)
        return m.matches()
    }
}

/**
 * ViewModel factory
 * @see LoginViewModel
 */
class LoginViewModelFactory :
    ViewModelProvider.Factory {
    /**
     * Creates the view model of the specific class
     *
     * @param modelClass The model class
     * @return An instance of the view model
     */
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return LoginViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}