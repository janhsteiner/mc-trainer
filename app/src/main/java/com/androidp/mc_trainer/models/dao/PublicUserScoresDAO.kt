package com.androidp.mc_trainer.models.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.androidp.mc_trainer.models.entities.PublicUserScores

/**
 * DAO for PublicUserScore entities
 *
 * @see PublicUserScores
 */
@Dao
interface PublicUserScoresDAO {
    companion object {
        private const val TABLE = "public_user_scores"
    }

    /**
     * Inserts a new PUS
     *
     * @param pus The PUS
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(pus: PublicUserScores)


    /**
     * Fetches the user scores of a given user
     *
     * @param userUid The User ID
     * @return List of PUS
     */
    @Query("SELECT * FROM $TABLE WHERE user_uid = :userUid")
    fun getUserScores(userUid: String): List<PublicUserScores>

    /**
     * Fetches the training scores for a user
     *
     * @param userUid       The User ID
     * @param trainingId    The training ID
     * @return List of PUS
     */
    @Query(
        "SELECT * FROM $TABLE WHERE user_uid = :userUid " +
                "AND training_id = :trainingId"
    )
    fun getTrainingScoresForUser(userUid: String, trainingId: Int): List<PublicUserScores>

    /**
     * Fetches the score of a single question
     *
     * @param userUid       The User ID
     * @param trainingId    The training ID
     * @param questionId    The question ID
     * @return The score of the question
     */
    @Query("SELECT score FROM $TABLE WHERE id = :questionId")
    fun getScore(questionId: Int): Int

    /**
     * Updates the score of a single question
     *
     * @param userUid       The User ID
     * @param trainingId    The training ID
     * @param questionId    The question ID
     * @param score         The score that should be assigned
     */
    @Query(
        "UPDATE $TABLE SET score = :score " +
                "AND question_id = :id"
    )
    fun updateScore(id: Int, score: Int)
}