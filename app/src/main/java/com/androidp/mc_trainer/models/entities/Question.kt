package com.androidp.mc_trainer.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * Represents a question
 *
 * Table name: questions
 * Fields:
 * - id            PK
 * - question_id    Position of the question inside the catalogue
 * - training_id    Corresponding training (FK)
 * - question       The title of the question itself
 * - answer_1       Answer no. 1
 * - correct_1      Answer no. 1 is correct
 * - answer_2       Answer no. 2
 * - correct_2      Answer no. 2 is correct
 * - answer_3       Answer no. 3
 * - correct_3      Answer no. 3 is correct
 * - answer_4       Answer no. 4
 * - correct_4      Answer no. 4 is correct
 * - answer_5       Answer no. 5
 * - correct_5      Answer no. 5 is correct
 * - answer_6       Answer no. 6
 * - correct_6      Answer no. 6 is correct
 */
@Entity(
    tableName = "questions",
    indices = [
        Index(
            "training_id",
            "question_id",
            unique = true
        )
    ]
)
data class Question(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "question_id") var questionId: Int,
    @ColumnInfo(name = "training_id") var trainingId: Int,
    @ColumnInfo(name = "question") var question: String,
    @ColumnInfo(name = "answer_1") var answer1: String,
    @ColumnInfo(name = "correct_1") var correct1: Boolean,
    @ColumnInfo(name = "answer_2") var answer2: String,
    @ColumnInfo(name = "correct_2") var correct2: Boolean,
    @ColumnInfo(name = "answer_3") var answer3: String,
    @ColumnInfo(name = "correct_3") var correct3: Boolean,
    @ColumnInfo(name = "answer_4") var answer4: String,
    @ColumnInfo(name = "correct_4") var correct4: Boolean,
    @ColumnInfo(name = "answer_5") var answer5: String?,
    @ColumnInfo(name = "correct_5") var correct5: Boolean?,
    @ColumnInfo(name = "answer_6") var answer6: String?,
    @ColumnInfo(name = "correct_6") var correct6: Boolean?,
    @ColumnInfo(name = "score") var score: Int
)