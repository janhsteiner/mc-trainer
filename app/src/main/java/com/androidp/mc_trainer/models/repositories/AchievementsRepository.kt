package com.androidp.mc_trainer.models.repositories

import androidx.annotation.WorkerThread
import com.androidp.mc_trainer.models.dao.AchievementDAO
import com.androidp.mc_trainer.models.entities.Achievement

/**
 * Wrapper class for AchievementDAO
 * @see AchievementDAO
 */
class AchievementsRepository(private val achievementDao: AchievementDAO) {
    /**
     * Wraps AchievementDao::getAllAchievements
     */
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun getAllAchievements(userUid: String): List<Achievement> {
        return achievementDao.getAllAchievements(userUid)
    }

    /**
     * Wraps AchievementDao::insert
     */
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(achievement: Achievement) =
        achievementDao.insert(achievement)

    /**
     * Wraps AchievementDao::achieved
     */
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun achieved(user: String, code: String) =
        achievementDao.achieved(user, code)
}
