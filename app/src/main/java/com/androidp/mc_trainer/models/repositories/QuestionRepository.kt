package com.androidp.mc_trainer.models.repositories

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.androidp.mc_trainer.models.dao.QuestionDAO
import com.androidp.mc_trainer.models.entities.Question

/**
 * Wrapper for QuestionDAO
 * @see QuestionDAO
 */
class QuestionRepository(private val questionDao: QuestionDAO) {
    /**
     * Wrapper method for [QuestionDAO.getQuestionsForTraining]
     */
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun getQuestionForTraining(id: Int): LiveData<List<Question>> {
        return questionDao.getQuestionsForTrainingSynced(id)
    }

    /**
     * Wrapper method for [QuestionDAO.getQuestionsForTrainingSynced]
     */
    fun getQuestionsForTraining(trainingId: Int): LiveData<List<Question>> {
        return questionDao.getQuestionsForTrainingSynced(trainingId)
    }

    /**
     * Wrapper method for [QuestionDAO.getQuestionsForCatalogueSynced]
     */
    fun getQuestionsForCatalogue(trainingId: Int): LiveData<List<Question>> {
        return questionDao.getQuestionsForCatalogueSynced(trainingId)
    }

    /**
     * Wrapper method for [QuestionDAO.getQuestionsForUser]
     */
    fun getQuestionsForUser(userUid: String): List<Int> {
        return questionDao.getQuestionsForUser(userUid)
    }

    /**
     * Wrapper method for [QuestionDAO.getQuestionsForTrainingSortedSynced]
     */
    fun getQuestionsForTrainingSortedSynced(id: Int): LiveData<List<Question>> {
        return questionDao.getQuestionsForTrainingSortedSynced(id)
    }

    /**
     * Wrapper method for [QuestionDAO.increaseScoreForQuestions]
     */
    fun increaseScoreForQuestion(questionIds: Int) =
        questionDao.increaseScoreForQuestions(questionIds)

    /**
     * Wrapper method for [QuestionDAO.resetScoreForQuestions]
     */
    fun resetScoreForQuestions(questionIds: Int) =
        questionDao.resetScoreForQuestions(questionIds)

    /**
     * Wrapper method for [QuestionDAO.getQuestion]
     */
    fun getQuestion(questionId: Int): Question =
        questionDao.getQuestion(questionId)

    /**
     * Wrapper method for [QuestionDAO.insert]
     */
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(q: Question) = questionDao.insert(q)

    /**
     * Wrapper method for [QuestionDAO.isQuestionPublic]
     *
     * @return true if question is assigned to a public training, false otherwise
     */
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun getUserForQuestion(trainingId: Int, questionId: Int): String =
        questionDao.getUserForQuestion(trainingId, questionId)
}