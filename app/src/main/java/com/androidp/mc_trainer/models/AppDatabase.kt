package com.androidp.mc_trainer.models

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.androidp.mc_trainer.models.dao.AchievementDAO
import com.androidp.mc_trainer.models.dao.PublicUserScoresDAO
import com.androidp.mc_trainer.models.dao.QuestionDAO
import com.androidp.mc_trainer.models.dao.TrainingDAO
import com.androidp.mc_trainer.models.entities.Achievement
import com.androidp.mc_trainer.models.entities.PublicUserScores
import com.androidp.mc_trainer.models.entities.Question
import com.androidp.mc_trainer.models.entities.Training

/**
 * Represents the room database of the application
 */
@Database(
    version = 22, entities = [
        Training::class,
        Question::class,
        Achievement::class,
        PublicUserScores::class
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun trainingDao(): TrainingDAO
    abstract fun questionDao(): QuestionDAO
    abstract fun achievementDao(): AchievementDAO
    abstract fun pusDao(): PublicUserScoresDAO

    companion object {
        private const val DB_NAME = "mc_trainer"

        @Volatile
        private var INSTANCE: AppDatabase? = null

        /**
         * Returns or creates a new database.
         * Only 1 database will exist at all times.
         *
         * @return the already existing instance of the AppDatabase ( [INSTANCE] ) or a new
         * instance
         * @param context, the application context
         */
        fun getDb(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    DB_NAME
                )
                    // destructive migration is ok, because we download everything from firebase again
                    .fallbackToDestructiveMigration()
                    .build()

                INSTANCE = instance
                instance
            }
        }
    }
}
