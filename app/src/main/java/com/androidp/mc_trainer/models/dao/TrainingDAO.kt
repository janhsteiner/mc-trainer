package com.androidp.mc_trainer.models.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.Query
import com.androidp.mc_trainer.models.entities.Training

/**
 * DAO for training entities
 *
 * @see Training
 */
@Dao
interface TrainingDAO {
    companion object {
        private const val TABLENAME = "trainings"
    }

    /**
     * Fetches all training modules
     *
     * @param userUid The User ID
     * @return List of trainings
     */
    @Query("SELECT * FROM $TABLENAME WHERE user_id IN (:userUid, 'public')")
    fun getAllTrainingsSynced(userUid: String): LiveData<List<Training>>

    /**
     * Fetches all training modules
     *
     * @param userUid The User ID
     * @return List of trainings
     */
    @Query("SELECT * FROM $TABLENAME WHERE user_id in (:userUid, 'public')")
    fun getAllTrainings(userUid: String): List<Training>

    /**
     * Fetches a single training
     *
     * @param trainingId The training ID
     * @return The training
     */
    @Query("SELECT * FROM $TABLENAME WHERE training_id = :trainingId LIMIT 1")
    fun getTraining(trainingId: Int): LiveData<Training>

    /**
     * Inserts a new training into the database. On conflict replaces the old training.
     *
     * @param t The training to be inserted
     */
    @Insert(onConflict = IGNORE)
    fun insert(t: Training)

    /**
     * Gets all training IDs
     *
     * @return List of all training IDs
     */
    @Query("SELECT id FROM $TABLENAME")
    fun getAllTrainingIds(): List<Int>
}