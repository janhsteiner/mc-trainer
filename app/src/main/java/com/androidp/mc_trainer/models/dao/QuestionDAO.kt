package com.androidp.mc_trainer.models.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.IGNORE
import androidx.room.Query
import com.androidp.mc_trainer.models.entities.Question

/**
 * DAO for question entities
 *
 * @see Question
 */
@Dao
interface QuestionDAO {
    companion object {
        private const val TABLENAME = "questions"
    }

    /**
     * Fetches all questions of a training
     *
     * @param id ID of the training
     * @return List of questions
     */
    @Query("SELECT * FROM $TABLENAME WHERE training_id == :id and score < 6 ORDER BY question_id")
    fun getQuestionsForTrainingSynced(id: Int): LiveData<List<Question>>

    /**
     * Fetches all questions of a training
     *
     * @param id ID of the training
     * @return List of questions
     */
    @Query("SELECT * FROM $TABLENAME WHERE training_id == :id ORDER BY question_id")
    fun getQuestionsForCatalogueSynced(id: Int): LiveData<List<Question>>

    /**
     * Fetches all questions of a training, sorted by their score ascending
     *
     * @param id ID of the training
     * @return List of questions
     */
    @Query("SELECT * FROM $TABLENAME WHERE training_id == :id and score < 6 ORDER BY score DESC")
    fun getQuestionsForTrainingSortedSynced(id: Int): LiveData<List<Question>>

    /**
     * Fetches all questions of a training
     *
     * @param id ID of the training
     * @return a list of questions
     */
    @Query("SELECT * FROM $TABLENAME WHERE training_id == :id and score < 6")
    fun getQuestionsForTraining(id: Int): List<Question>

    /**
     * Increases the score of each question
     *
     * @param questionIds List containing the question IDs
     */
    @Query("UPDATE $TABLENAME SET score = MIN(score+1, 6) WHERE id == :questionIds")
    fun increaseScoreForQuestions(questionIds: Int)

    /**
     * Sets the score of each question to zero
     *
     * @param questionIds List containing the question IDs
     */
    @Query("UPDATE $TABLENAME SET score = 0 WHERE id == :questionIds")
    fun resetScoreForQuestions(questionIds: Int)

    /**
     * Inserts a new question into the database. On conflict ignores the existing question.
     *
     * @param q The question to be inserted
     */
    @Insert(onConflict = IGNORE)
    fun insert(q: Question)

    /**
     * Gets all question IDs
     *
     * @return List of all question IDs
     */
    @Query("SELECT id FROM $TABLENAME")
    fun getAllQuestionIds(): List<Int>

    /**
     * Gets a list of questions assigned to this user
     *
     * @param userUid the user you want to get the questions for
     */
    @Query(
        "SELECT q.id FROM $TABLENAME q " +
                "INNER JOIN trainings t ON q.training_id = t.training_id " +
                "WHERE t.user_id = :userUid"
    )
    fun getQuestionsForUser(userUid: String): List<Int>

    /**
     * Gets the questions with the given id
     *
     * @param id the unique question id
     */
    @Query("SELECT * FROM $TABLENAME WHERE id == :id")
    fun getQuestion(id: Int): Question

    @Query(
        "SELECT user_id FROM $TABLENAME q " +
                "INNER JOIN trainings t ON t.training_id = q.training_id " +
                "WHERE q.training_id = :trainingId AND " +
                "q.question_id = :questionId"
    )
    fun getUserForQuestion(trainingId: Int, questionId: Int): String

    /**
     * Gets the highest question id
     *
     * @return the highest question id
     */
    @Query("SELECT MAX(id) from $TABLENAME")
    fun getHighestId(): Int
}
