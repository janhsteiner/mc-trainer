package com.androidp.mc_trainer.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * Entity for achievements
 *
 * Table name: achievements
 * Fields:
 * - id:        PK
 * - user_uid:  Corresponding user ID
 * - code:      Achievement code
 * - achieved:  Did the user achieve this achievement?
 * @see AchievementsEnum
 */
@Entity(
    tableName = "achievements",
    indices = [
        Index(
            "user_uid",
            "code",
            unique = true
        )
    ]
)
data class Achievement(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "user_uid") var userUid: String,
    @ColumnInfo(name = "code") var code: String,
    @ColumnInfo(name = "achieved") var achieved: Boolean
)
