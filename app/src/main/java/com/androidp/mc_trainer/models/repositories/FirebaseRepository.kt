package com.androidp.mc_trainer.models.repositories

import android.os.Environment
import android.util.Log
import com.androidp.mc_trainer.models.entities.Achievement
import com.androidp.mc_trainer.models.entities.PublicUserScores
import com.androidp.mc_trainer.models.entities.Question
import com.androidp.mc_trainer.models.entities.Training
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.getField
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.File
import java.io.FileOutputStream
import java.lang.Thread.sleep


/**
 * This class handles the download of the database from Firebase Cloud Firestore and
 * imports them into our local Room database
 */
class FirebaseRepository {

    private val TAG = "FirebaseData"
    private val firebaseDb = Firebase.firestore
    private var userDocId = "public"
    private val trainings = mutableListOf<Training>()
    private lateinit var trainingQuery: Task<QuerySnapshot>

    /**
     * Downloads the data from the firebase database and inserts it directly into our local room
     * database.
     * Every training and its thumbnail and question for the current user, as well as for the
     * public user, will be downloaded and saved.
     * Achievements and scores for public trainings will be downloaded too.
     *
     * Calls [downloadTrainings], [downloadQuestions], [downloadAchievements], [downloadScores] and
     * [downloadThumbnails].
     *
     * @param userUID the current user
     * @param trainingRepository the training repository
     * @param questionRepository the question repository
     * @param achievementsRepository the achievement repository
     * @param pusRepository the public user score repository
     */
    fun syncDatabasesForUser(
        userUID: String,
        trainingRepository: TrainingRepository,
        questionRepository: QuestionRepository,
        achievementsRepository: AchievementsRepository,
        pusRepository: PublicUserScoresRepository
    ) {
        try {
            for (uid in listOf(userUID, "public")) {
                downloadTrainings(uid, trainingRepository)

                while (!trainingQuery.isComplete) {
                    sleep(20)
                }

                val thumbnailList = mutableListOf<String>()
                for (t in trainings) {
                    downloadQuestions(t.userId, t.trainingId, questionRepository)
                    thumbnailList.add(t.imageName)
                }

                if (uid != "public") {
                    downloadAchievements(uid, achievementsRepository)
                    downloadScores(uid, pusRepository)
                }

                downloadThumbnails(thumbnailList)
            }
        } catch (e: ConcurrentModificationException) {
            e.printStackTrace()
        }
    }

    /**
     * Uploads the data from our local room database to the firebase database.
     * Calls [updateAchievements] and [updateScores]
     *
     * @param userUID the current user used for both methods
     * @param questionRepo the question repository
     * @param achievementsRepository the achievement repository
     * @param pusRepository the public user scores repository
     */
    fun uploadDataToFirebase(
        userUID: String,
        questionRepo: QuestionRepository,
        achievementsRepository: AchievementsRepository,
        pusRepository: PublicUserScoresRepository
    ) {
        updateAchievements(userUID, achievementsRepository)
        updateScores(userUID, questionRepo, pusRepository)
    }

    /**
     * Downloads every training for this user from the firebase database.
     * Immediatly after the download finished, these trainings will be inserted into our local
     * room database.
     *
     * If an error occurs, an exception wll be printed and nothing will be inserted.

     * @param userUID the current user
     * @param repo the training repository
     */
    private fun downloadTrainings(userUID: String, repo: TrainingRepository) {
        // get document id for this user
        // if the user is not present (res is empty) the default value 0 will be used
        trainingQuery = firebaseDb
            .collection("users/$userUID/trainings")
            .get()
            .addOnSuccessListener { result ->
                if (result == null || result.isEmpty) {
                    return@addOnSuccessListener
                }

                for (t in result) {
                    when (
                        t["id"] != null &&
                                t["training_name"] != null &&
                                t["image_name"] != null) {
                        true -> {
                            trainings.add(
                                Training(
                                    t.getField<Int>("id")!!,
                                    userUID,
                                    t.getString("training_name")!!,
                                    t.getString("image_name")!!
                                )
                            )
                        }
                        else -> continue
                    }
                }

                // immediately add the trainings into our room database
                val job = CoroutineScope(Dispatchers.IO).launch {
                    trainings.forEach { repo.insert(it) }
                }

                runBlocking {
                    job.join()
                }
            }
    }


    /**
     * Downloads the questions for this training for this user from the firebase database.
     * Immediatly afte the download finished, the questions will be inserte into the local room
     * db.
     *
     * If an error occurs, an exception wll be printed and nothing will be inserted.
     *
     * @param userUid the uid provided by firebase of the current user
     * @param trainingId the training id
     * @param repo the question repository
     */
    private fun downloadQuestions(userUid: String, trainingId: Int, repo: QuestionRepository) {
        val questions = mutableListOf<Question>()

        firebaseDb
            .collection("users/$userUid/trainings/$trainingId/questions")
            .get()
            .addOnFailureListener {
                Log.e(TAG, "downloadQuestions(): Error while downloading data")
                it.printStackTrace()

            }
            .addOnSuccessListener { res ->
                if (res != null && !res.isEmpty) {

                    res.documents.forEach { q ->
                        questions.add(
                            Question(
                                0,
                                q.getField<Int>("id")!!,
                                trainingId,
                                q.getString("question")!!,
                                q.getString("answer_1")!!,
                                q.getBoolean("correct_1")!!,
                                q.getString("answer_2")!!,
                                q.getBoolean("correct_2")!!,
                                q.getString("answer_3")!!,
                                q.getBoolean("correct_3")!!,
                                q.getString("answer_4")!!,
                                q.getBoolean("correct_4")!!,
                                q.getString("answer_5"),
                                q.getBoolean("correct_5"),
                                q.getString("answer_6"),
                                q.getBoolean("correct_6"),
                                q.getField<Int>("score")!!
                            )
                        )
                    }
                    CoroutineScope(Dispatchers.IO).launch {
                        for (qu in questions) {
                            repo.insert(qu)
                        }
                    }
                }
            }
            .addOnFailureListener {
                Log.e(TAG, "downloadQuestions: Error during downloading of questions")
                it.printStackTrace()
            }
    }

    /**
     * This method downloads the achievements for this user and immediatly inserted into the room
     * db. The achievements are only inserted, if it does'nt exist yet.
     *
     * If an error during the download occurs, an exception will be printed and nothing will be
     * inserted.
     *
     * @param userUID the current user
     * @param repo the achievements repository
     */
    private fun downloadAchievements(userUID: String, repo: AchievementsRepository) {
        val achievementList = mutableListOf<Achievement>()

        firebaseDb
            .collection("users/$userUID/achievements")
            .get()
            .addOnSuccessListener { res ->
                if (res == null || res.isEmpty) return@addOnSuccessListener

                for (a in res) {
                    achievementList.add(
                        Achievement(
                            0,
                            userUID,
                            a.id,
                            a.getBoolean("achieved") ?: false
                        )
                    )
                }

                CoroutineScope(Dispatchers.IO).launch {
                    achievementList.forEach { repo.insert(it) }
                }
            }
            .addOnFailureListener {
                Log.e(TAG, "downloadAchievements: Error during download of achievements")
                it.printStackTrace()
            }
    }

    /**
     * Downloads a template of public scores for this user.
     * Template because the values in our room database will not be overwritten by the
     * downloaded ones from firebase.
     *
     * If an error during the download occurs, an exception will be thrown and catched. In this case
     * nothing else happens and nothing will be inserted.
     * Same with the transformation, but a [NumberFormatException] will be thrown and catched.
     *
     * @param userUID the current user
     * @param repo the public user scores repository
     */
    private fun downloadScores(userUID: String, repo: PublicUserScoresRepository) {
        val allScores = mutableListOf<MutableMap<String, Long>>()

        firebaseDb
            .collection("users/$userUID/public_training_scores")
            .get()
            .addOnSuccessListener { res ->
                res.documents.forEach { doc ->
                    val data = mutableMapOf<String, Long>()
                    doc.data?.forEach { it ->
                        data[it.key] = it.value as Long
                    }

                    allScores.add(data)
                }

                CoroutineScope(Dispatchers.IO).launch {
                    try {
                        allScores.forEach {
                            val trainingId: Int = it["training_id"]?.toInt() ?: 0
                            it.filter { map -> map.key != "training_id" }
                                .forEach { mapEntry ->
                                    repo.insert(
                                        PublicUserScores(
                                            0,
                                            userUID,
                                            trainingId,
                                            mapEntry.key.toInt(),
                                            mapEntry.value.toInt()
                                        )
                                    )
                                }
                        }
                    } catch (e: NumberFormatException) {
                        e.printStackTrace()
                        Log.e(TAG, "downloadScores: Error during insert of scores")
                    }
                }
            }
            .addOnFailureListener {
                Log.e(TAG, "downloadQuestions(): Error while downloading data")
                it.printStackTrace()
            }
    }

    /**
     * Downloads every image from Firebase Storage and inserts them into the files directory
     * of the application. If this directory does not exist, it will be created.
     * Note: Images will only be saved if they don't exist, disabling updates!
     *
     * @param imageList the list of image names. The image name will be taken from the image name
     * in training.
     */
    private fun downloadThumbnails(imageList: List<String>) {
        val storage = Firebase.storage.reference

        for (imageName in imageList) {
            val imageRef = storage.child(imageName)

            // internal storage path. In Device File Explorer this would be
            // 'data/data/com.androidp.mc_trainer/files'
            val path = Environment.getDataDirectory().absolutePath +
                    "/data/com.androidp.mc_trainer/files/"
            var f = File(path)

            // if this directory does not exist, create it
            if (!f.exists() && !f.isDirectory)
                f.mkdir()

            // if this file already exists, we don't want to overwrite it
            f = File(path + imageName)
            if (f.exists())
                break

            // download maximum 20 MB of data,because we don't want the file to be too big
            val job = imageRef.getBytes(20 * (1024 * 1024))
                .addOnSuccessListener { imageByteArray ->

                    // save new file to folder 'files'
                    f.createNewFile()
                    val out = FileOutputStream(path + imageName)
                    out.write(imageByteArray)
                    out.close()
                }
                .addOnFailureListener {
                    it.printStackTrace()
                    Log.e(TAG, "downloadThumbnails: Error during download")
                }

            // wait for download to finish
            while (!job.isComplete) {
                if (job.isCanceled)
                    break

                sleep(20)
            }
        }
    }

    /**
     * Inserts the values from the local room db into firebase.
     *
     * @param userUID a string representing the current user.
     * It must be exactly the same as the userUid provided by Firebase Auth
     *
     * @param repo the achievements repository
     */
    private fun updateAchievements(
        userUID: String,
        repo: AchievementsRepository
    ) {
        val localAchievements = mutableListOf<Achievement>()
        CoroutineScope(Dispatchers.IO).launch {
            localAchievements.addAll(repo.getAllAchievements(userUID))
        }.invokeOnCompletion {
            localAchievements.forEach {
                val m = mapOf("achieved" to it.achieved)

                firebaseDb
                    .collection("users/$userUID/achievements")
                    .document(it.code)
                    .set(m)
            }
        }
    }

    /**
     * Updates the score for public and private trainings for this user.
     * Both scores are taken from our local room database, transformed and uploaded to firebase.
     *
     * @param userUid the current user
     * @param questionRepo the question repository
     * @param pusRepo the public user scores repository
     */
    private fun updateScores(
        userUid: String,
        questionRepo: QuestionRepository,
        pusRepo: PublicUserScoresRepository
    ) {
        // public trainings
        var localScores: List<PublicUserScores>? = null
        var trainingIdList: List<Int>

        CoroutineScope(Dispatchers.IO).launch {
            localScores = pusRepo.getUserScores(userUid)
        }.invokeOnCompletion { _ ->
            // get a list of all training ids
            trainingIdList = localScores!!
                .distinctBy { it.trainingId }
                .map { it.trainingId }

            trainingIdList.forEach { tid ->
                val scoresMap = mutableMapOf<String, Int>()

                // map the question id to its score
                scoresMap.putAll(
                    localScores!!
                        .filter { it.trainingId == tid }
                        .map { it.questionId.toString() to it.score }
                )
                // append the trainings id
                scoresMap["training_id"] = tid

                // upload
                firebaseDb.collection("users/$userUid/public_training_scores")
                    .document(tid.toString())
                    .set(scoresMap)
            }
        }

        // private trainings
        val privateScores: MutableList<Question> = mutableListOf()
        CoroutineScope(Dispatchers.IO).launch {
            // get local scores for private trainings
            val questionIdList = questionRepo.getQuestionsForUser(userUid)
            questionIdList.forEach { privateScores.add(questionRepo.getQuestion(it)) }
        }.invokeOnCompletion {
            privateScores.forEach {
                var ref = ""
                // get document reference of question for this training and user
                firebaseDb.collection("users/$userUid/trainings")
                    .document(it.toString())
                    .collection("questions")
                    .whereEqualTo("id", it)
                    .addSnapshotListener { value, _ ->
                        ref = value?.documents?.getOrNull(0).toString()
                    }

                if (ref.isNotBlank() && ref != "null") {
                    // update score of ref
                    firebaseDb
                        .collection("users/$userUid/trainings/${it.trainingId}/questions")
                        .document(ref)
                        .set(
                            mapOf("score" to it.score)
                        )
                }
            }
        }
    }
}