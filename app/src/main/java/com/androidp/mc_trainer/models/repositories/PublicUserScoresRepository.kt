package com.androidp.mc_trainer.models.repositories

import androidx.annotation.WorkerThread
import com.androidp.mc_trainer.models.dao.PublicUserScoresDAO
import com.androidp.mc_trainer.models.entities.PublicUserScores

/**
 * Wrapper class for [PublicUserScoresDAO]
 * @see dao the PublicUserScoresDAO
 */
class PublicUserScoresRepository(private val dao: PublicUserScoresDAO) {
    /**
     * Wrapper method for [PublicUserScoresDAO.insert]
     */
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(score: PublicUserScores) {
        return dao.insert(score)
    }

    /**
     * Wrapper method for [PublicUserScoresDAO.getUserScores]
     */
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun getUserScores(userUid: String): List<PublicUserScores> {
        return dao.getUserScores(userUid)
    }

    /**
     * Wrapper method for [PublicUserScoresDAO.getScore]
     */
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun getScore(questionId: Int): Int {
        return dao.getScore(questionId)
    }

    /**
     * Wrapper method for [PublicUserScoresDAO.updateScore]
     */
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun incrementScore(questionId: Int) {
        val score = dao.getScore(questionId) + 1
        return dao.updateScore(questionId, score)
    }

    /**
     * Wrapper method for [PublicUserScoresDAO.updateScore]
     */
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun resetScore(questionId: Int) {
        return dao.updateScore(questionId, 0)
    }
}