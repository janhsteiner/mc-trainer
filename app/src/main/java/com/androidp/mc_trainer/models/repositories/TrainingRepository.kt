package com.androidp.mc_trainer.models.repositories

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.androidp.mc_trainer.models.dao.TrainingDAO
import com.androidp.mc_trainer.models.entities.Training

/**
 * Wrapper for TrainingDAO
 * @see TrainingDAO
 */
class TrainingRepository(private val trainingDao: TrainingDAO) {
    /**
     * Wrapper method
     */
    @WorkerThread
    fun getAllTrainingsSynced(userUid: String): LiveData<List<Training>> {
        return trainingDao.getAllTrainingsSynced(userUid)
    }

    /**
     * Wrapper method
     */
    @WorkerThread
    fun getTraining(trainingId: Int): LiveData<Training> {
        return trainingDao.getTraining(trainingId)
    }

    /**
     * Wrapper method
     */
    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(t: Training) = trainingDao.insert(t)
}