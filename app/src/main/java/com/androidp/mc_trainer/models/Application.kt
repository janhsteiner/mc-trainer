package com.androidp.mc_trainer.models

import android.app.Application
import com.androidp.mc_trainer.models.repositories.*

/**
 * Represents the state of the application
 */
class Application : Application() {
    val db by lazy { AppDatabase.getDb(this) }
    val trainingRepository by lazy { TrainingRepository(db.trainingDao()) }
    val questionRepository by lazy { QuestionRepository(db.questionDao()) }
    val firebaseRepository by lazy { FirebaseRepository() }
    val achievementsRepository by lazy { AchievementsRepository(db.achievementDao()) }
    val pusRepository by lazy { PublicUserScoresRepository(db.pusDao()) }
}
