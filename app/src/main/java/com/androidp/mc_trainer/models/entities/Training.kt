package com.androidp.mc_trainer.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * This entity represents a training module.
 *
 * table name: modules
 * fields:
 * 1. id (PK)
 * 2. module_name
 * 3. image_name (represents the path to the internal storage where the image will be stored,
 *  image names should follow the pattern: thumbnail_userId_id to accurately identify the
 *  images)
 */
@Entity(
    tableName = "trainings",
    indices = [
        Index(
            "training_id",
            "user_id",
            unique = true
        )
    ]
)

class Training(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @ColumnInfo(name = "training_id") var trainingId: Int,
    @ColumnInfo(name = "user_id") var userId: String,
    @ColumnInfo(name = "training_name") var moduleName: String,
    @ColumnInfo(name = "image_name") var imageName: String
) {

    constructor(trainingId: Int, userId: String, trainingName: String, imageName: String)
            : this(0, trainingId, userId, trainingName, imageName)
}