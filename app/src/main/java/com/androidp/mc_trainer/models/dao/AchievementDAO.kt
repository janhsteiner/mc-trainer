package com.androidp.mc_trainer.models.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.androidp.mc_trainer.models.entities.Achievement

/**
 * DAO for achievement entities
 *
 * @see Achievement
 */
@Dao
interface AchievementDAO {
    companion object {
        private const val TABLE = "achievements"
    }

    /**
     * Fetches all achievements of a given user
     *
     * @param userUid User ID
     * @return List of achievements
     */
    @Query("SELECT * FROM $TABLE where user_uid == :userUid")
    fun getAllAchievements(userUid: String): List<Achievement>

    /**
     * Inserts a new achievement. Ignores conflicts.
     *
     * @param achievement The achievement to insert
     */
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(achievement: Achievement)

    /**
     * Achieves an achievement
     *
     * @param user The user ID
     * @param code The code of the achievement
     */
    @Query("UPDATE $TABLE SET achieved = 1 where user_uid = :user and code = :code")
    fun achieved(user: String, code: String)
}
