package com.androidp.mc_trainer.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

/**
 * Represents the user score for a public training module
 *
 * Table name: public_user_scores
 * Fields:
 * - id:            PK
 * - user_uid:      Corresponding User ID
 * - training_id:   training ID
 * - question_id:   question ID
 * - score:         the score
 */
@Entity(
    tableName = "public_user_scores",
    indices = [
        Index(
            "user_uid",
            "training_id",
            "question_id",
            unique = true
        )
    ]
)
class PublicUserScores(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "user_uid") var userUid: String,
    @ColumnInfo(name = "training_id") var trainingId: Int,
    @ColumnInfo(name = "question_id") var questionId: Int,
    @ColumnInfo(name = "score") var score: Int
)