package com.androidp.mc_trainer_backend

import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ListUsersPage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class TrainingFragment : Fragment() {

    private lateinit var firebaseAuth: FirebaseAuth
    private val TAG = "FirstFragment"
    private val checkedUIDs = mutableListOf<String>()

    private lateinit var trainingNameET: EditText
    private lateinit var selectedUsersText: TextView
    private lateinit var mainActivity: MainActivity

    private val IMAGE_OK = 100

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_training, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // these elements have to be available outside of method
        trainingNameET = view.findViewById(R.id.trainig_name_edittext)
        selectedUsersText = view.findViewById(R.id.selected_users_text)

        // these don't
        val showAllUsersButton = view.findViewById<Button>(R.id.select_users_button)
        val selectImageButton = view.findViewById<Button>(R.id.select_image_button)
        val nextScreenButton = view.findViewById<Button>(R.id.next_screen_button)
        this.mainActivity = activity as MainActivity

        // button is disabled by default
        showAllUsersButton.isEnabled = false
        var showAllUsersButtonClick = View.OnClickListener {
            Toast.makeText(
                context,
                "Please wait while users are loaded",
                Toast.LENGTH_SHORT
            ).show()
        }
        showAllUsersButton.setOnClickListener(showAllUsersButtonClick)


        // get first page (first 1000 entires) users in asynchronous request
        // i doubt that we will have more 1000 users, so i didn't fetch more
        var externalUsers: ListUsersPage? = null
        CoroutineScope(Dispatchers.IO).launch {
            firebaseAuth = FirebaseAuth.getInstance()
            externalUsers = firebaseAuth.listUsers(null)
        }.invokeOnCompletion {
            CoroutineScope(Dispatchers.Main).launch {
                // once the users are available, enable the button
                showAllUsersButton.isEnabled = true
                showAllUsersButtonClick = View.OnClickListener {
                    showSingleSelectAlertDialog(externalUsers)
                }
                showAllUsersButton.setOnClickListener(showAllUsersButtonClick)
            }
        }

        // open gallery when selectImageButton is clicked
        selectImageButton.setOnClickListener {
            val openGallery = Intent(Intent.ACTION_PICK)
            openGallery.type = "image/*"
            startActivityForResult(openGallery, IMAGE_OK)
        }

        nextScreenButton.setOnClickListener {
            mainActivity.trainingName = trainingNameET.text.toString()

            parentFragmentManager.commit {
                remove(TrainingFragment())
                replace(R.id.fragment_container, QuestionFragment())
            }

            this.onStop()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, galleryIntent: Intent?) {
        super.onActivityResult(requestCode, resultCode, galleryIntent)
        if (resultCode == RESULT_OK && requestCode == IMAGE_OK) {
            if (galleryIntent?.data != null) {
                mainActivity.imagePath = galleryIntent.data!!
                val cr = context?.contentResolver
                mainActivity.imageType = cr?.getType(mainActivity.imagePath)
            }
        }
    }

    private fun showSingleSelectAlertDialog(userListPage: ListUsersPage?) {
        if (userListPage == null) return

        val s = StringBuilder()
        val userUids = mutableListOf<String>()
        userUids.add("public")

        val userStrings = mutableListOf<CharSequence>()
        userStrings.add("public")

        userListPage.iterateAll()?.forEach {
            userUids.add(it.uid)

            userStrings.add(
                when (it.displayName != null) {
                    true -> it.displayName
                    false -> it.email
                }
            )
        }

        AlertDialog
            .Builder(context)
            .setTitle(R.string.alert_dialog_title)
            .setSingleChoiceItems(
                userStrings.toTypedArray(),
                -1
            ) { dialog, which ->
                mainActivity.selectedUserUID = userUids[which]
                selectedUsersText.text = mainActivity.selectedUserUID
                dialog.dismiss()
            }
            .setNegativeButton(R.string.alert_dialog_negative) { dialog, _ ->
                checkedUIDs.clear()
                dialog.dismiss()
            }
            .show()

        selectedUsersText.text = s.toString()
    }
}
