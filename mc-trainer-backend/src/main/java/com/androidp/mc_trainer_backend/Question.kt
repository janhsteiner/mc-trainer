package com.androidp.mc_trainer_backend

data class Question(
    val id: Int,
    val question: String,
    val answer1: String,
    val correct1: Boolean,
    val answer2: String,
    val correct2: Boolean,
    val answer3: String,
    val correct3: Boolean,
    val answer4: String,
    val correct4: Boolean,
    val answer5: String?,
    val correct5: Boolean?,
    val answer6: String?,
    val correct6: Boolean?,
    val score: Int = 0
)
