package com.androidp.mc_trainer_backend

import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.firestore.Firestore
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.cloud.FirestoreClient.getFirestore
import com.google.firebase.cloud.StorageClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    var trainingName = ""
    var selectedUserUID = ""
    lateinit var imagePath: Uri
    var imageName = ""
    var questionList = mutableListOf<Question>()
    var tableName = "trainingstest"
    var trainingId = 0
    var imageType: String? = ""
    lateinit var firebaseApp: FirebaseApp
    lateinit var firestore: Firestore
    lateinit var firestorage: StorageClient
    lateinit var sharedPrefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        sharedPrefs = applicationContext.getSharedPreferences("trainingCounter", 0)

        // open config file
        val fis = resources.openRawResource(R.raw.serviceaccountkey)

        // set firebaseOptions from assets > serviceaccountkey.json
        val options = FirebaseOptions.builder()
            .setCredentials(GoogleCredentials.fromStream(fis))
            .setDatabaseUrl("https://mc-trainer-85c5b-default-rtdb.europe-west1.firebasedatabase.app")
            .setStorageBucket("mc-trainer-85c5b.appspot.com")
            .build()

        CoroutineScope(Dispatchers.IO).launch {
            firebaseApp = FirebaseApp.initializeApp(options)
            firestore = getFirestore(firebaseApp)
            firestorage = StorageClient.getInstance(firebaseApp)
        }.invokeOnCompletion {
            supportFragmentManager.commit {
                replace<TrainingFragment>(R.id.fragment_container)
            }
        }
    }
}