package com.androidp.mc_trainer_backend

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.google.cloud.storage.StorageException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserRecord
import kotlinx.coroutines.*
import java.io.FileNotFoundException
import java.io.InputStream
import java.util.*

class QuestionFragment : Fragment() {

    private lateinit var answers: TreeMap<String, EditText>
    private lateinit var corrects: TreeMap<String, CheckBox>
    private lateinit var questionText: EditText
    private lateinit var mainActivity: MainActivity
    private var questionCounter: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_question, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val finishButton = view.findViewById<Button>(R.id.finish)
        val nextButton = view.findViewById<Button>(R.id.next_question)

        mainActivity = activity as MainActivity
        questionText = view.findViewById(R.id.enter_question_textarea)
        answers = TreeMap<String, EditText>()

        // store edittexts in a map for easier access
        answers["answer1"] = view.findViewById(R.id.answer1)
        answers["answer2"] = view.findViewById(R.id.answer2)
        answers["answer3"] = view.findViewById(R.id.answer3)
        answers["answer4"] = view.findViewById(R.id.answer4)
        answers["answer5"] = view.findViewById(R.id.answer5)
        answers["answer6"] = view.findViewById(R.id.answer6)

        // store checkboxes in a map for easier access
        corrects = TreeMap<String, CheckBox>()
        corrects["correct1"] = view.findViewById(R.id.correct1)
        corrects["correct2"] = view.findViewById(R.id.correct2)
        corrects["correct3"] = view.findViewById(R.id.correct3)
        corrects["correct4"] = view.findViewById(R.id.correct4)
        corrects["correct5"] = view.findViewById(R.id.correct5)
        corrects["correct6"] = view.findViewById(R.id.correct6)

        nextButton.setOnClickListener {
            if (!addQuestion())
                return@setOnClickListener

            questionCounter++

            // clear elements
            questionText.setText("")
            answers.forEach { it.value.setText("") }
            corrects.forEach { it.value.isChecked = false }
        }

        finishButton.setOnClickListener {
            // store the id of the to be inserted image in the shared preferences
            val trainingCounter =
                mainActivity
                    .sharedPrefs
                    .getInt("trainingCounter",0)

            mainActivity
                .sharedPrefs
                .edit()
                .putInt("trainingCounter", trainingCounter + 1)
                .apply()


            val type: String? = mainActivity.imageType?.split("/")?.get(1)
            mainActivity.imageName =
                "thumbnail_$trainingCounter.$type"

            var currentUid = mainActivity.selectedUserUID
            var currentUser: UserRecord?
            var trainingId = 0

            CoroutineScope(Dispatchers.IO).launch {
                currentUser = if(currentUid != "public") {
                    FirebaseAuth.getInstance().getUser(currentUid)
                } else {
                    null
                }

                currentUid = currentUser?.uid ?: "public"

                // create user
                mainActivity.firestore
                    .collection("users")
                    .document(currentUid)
                    .set(mapOf(
                        "user" to (currentUser?.email ?: "public")
                    ))

                trainingId = trainingCounter

                mainActivity.firestore
                    .collection("users/${currentUid}/trainings")
                    .document(trainingId.toString())
                    .set(
                        mapOf(
                            "id" to trainingId,
                            "training_name" to mainActivity.trainingName,
                            "image_name" to mainActivity.imageName
                        )
                    )
            }.invokeOnCompletion {
                CoroutineScope(Dispatchers.IO).launch {
                    for (q in mainActivity.questionList) {
                        // use auto-generated id to add a new collection consisting of questions
                        // to this training
                        mainActivity.firestore
                            .collection("users/$currentUid/trainings/$trainingId/questions")
                            .add(
                                mapOf(
                                    "id" to q.id,
                                    "question" to q.question,
                                    "answer_1" to q.answer1,
                                    "answer_2" to q.answer2,
                                    "answer_3" to q.answer3,
                                    "answer_4" to q.answer4,
                                    "answer_5" to q.answer5,
                                    "answer_6" to q.answer6,
                                    "correct_1" to q.correct1,
                                    "correct_2" to q.correct2,
                                    "correct_3" to q.correct3,
                                    "correct_4" to q.correct4,
                                    "correct_5" to q.correct5,
                                    "correct_6" to q.correct6,
                                    "score" to 0
                                )
                            )
                    }

                    // list of achievements in mc_trainer/AchievementsEnum.kt
                    val list = listOf(
                        "newbie",
                        "veteran",
                        "clever",
                        "nerd",
                        "unlucky"
                    )

                    // insert achievements for every user except 'public'
                    if(currentUid != "public") {
                        val exists = mainActivity.firestore
                            .collection("users/$currentUid/achievements")
                            .document("achievements")
                            .listCollections()
                            .count() != 0

                        // and only if it doesn't exist yet, because set() overwrites any
                        // existing variables
                        if (!exists) {
                            for (a in list) {
                                mainActivity.firestore
                                    .collection("users/$currentUid/achievements")
                                    .document(a)
                                    .set(
                                        mapOf(
                                            "achieved" to false
                                        ),
                                    )
                            }
                        }
                    }

                // if the current training is a public training we have to insert
                // a public scores table
                // and if we insert the users first private training, the user won't have the scores
                // table from any public training before now, so we update the scores on insert,
                // but only if they don't exist yet
                mainActivity.firestore
                    .collection("users/public/trainings")
                    .listDocuments()
                    .forEach {
                        val nrOfQuestions =
                            it.collection("questions")
                                .listDocuments()
                                .count()

                        val questionMap = mutableMapOf<String, Any>()
                        questionMap["training_id"] =
                            it.get().get().get("id")!!

                        for(i in 0 until nrOfQuestions)
                            questionMap[i.toString()] = 0

                        // insert scores for questions in public trainings for every user
                        // except 'public'
                        mainActivity.firestore
                            .collection("users")
                            .listDocuments()
                            .forEach { res ->
                                // set() overwrites any changes so we have to check if they  exist
                                val scoresExist =
                                    res.collection("public_training_scores")
                                        .whereEqualTo(
                                            "training_id",
                                            questionMap.getOrDefault("training_id", -1)
                                        )
                                        .get().get().size() > 0

                                if (res.id != "public" && !scoresExist ) {
                                    res.collection("public_training_scores")
                                        .document(questionMap["training_id"].toString())
                                        .set(questionMap)
                                }
                            }
                    }
                }
            }

            // get image as stream
            val imageStream: InputStream?
            try {
                imageStream = context?.contentResolver?.openInputStream(mainActivity.imagePath)
            } catch (ioe: FileNotFoundException) {
                ioe.printStackTrace()
                Toast.makeText(
                    context,
                    "Error: File ${mainActivity.imageName} not found!",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            // upload image to firebase storage
            CoroutineScope(Dispatchers.IO).launch {
                val bucket = mainActivity.firestorage.bucket()

                try {
                    val b =
                        bucket.create(mainActivity.imageName, imageStream!!, mainActivity.imageType)

                    withContext(Dispatchers.Main) {
                        if (b != null) {
                            Toast.makeText(
                                context,
                                "Upload successful!",
                                Toast.LENGTH_SHORT
                            ).show()

                            mainActivity.questionList.clear()

                            // redirect to TrainingFragment
                            parentFragmentManager.commit {
                                remove(QuestionFragment())
                                replace(R.id.fragment_container, TrainingFragment())
                            }

                        }
                    }
                } catch (e: StorageException) {
                    Toast.makeText(
                        context,
                        "Upload failed!",
                        Toast.LENGTH_SHORT
                    ).show()
                    e.printStackTrace()
                }
            }
        }
    }

    private fun addQuestion(): Boolean {
        if (questionText.text.toString().trim() == "" ||
            answers.getValue("answer1").text.toString().trim() == "" ||
            answers.getValue("answer2").text.toString().trim() == "" ||
            answers.getValue("answer3").text.toString().trim() == "" ||
            answers.getValue("answer4").text.toString().trim() == ""
        ) {
            Toast.makeText(
                context,
                "Einige erforderliche Felder sind noch nicht ausgefüllt",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }

        if (corrects.values.count { c -> c.isChecked } == 0) {
            Toast.makeText(
                context,
                "Es muss min. 1 richtige Antwort ausgewählt werden",
                Toast.LENGTH_SHORT
            ).show()
            return false
        }

        mainActivity.questionList.add(
            Question(
                questionCounter,
                questionText.text.toString().trim(),
                answers.getValue("answer1").text.toString().trim(),
                corrects.getValue("correct1").isChecked,
                answers.getValue("answer2").text.toString().trim(),
                corrects.getValue("correct2").isChecked,
                answers.getValue("answer3").text.toString().trim(),
                corrects.getValue("correct3").isChecked,
                answers.getValue("answer4").text.toString().trim(),
                corrects.getValue("correct4").isChecked,
                answers.getValue("answer5").text.toString().trim(),
                corrects.getValue("correct5").isChecked,
                answers.getValue("answer6").text.toString().trim(),
                corrects.getValue("correct6").isChecked,
                0
            )
        )

        return true
    }
}