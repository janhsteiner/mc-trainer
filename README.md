# MC-Trainer

## Einleitung
Im Rahmen der Veranstaltung "Android-Projekt" wurde eine Android-App zum Üben
von Multiple-Choice-Fragen entwickelt.

MC-Trainer ist ein universeller Multiple-Choice-Trainer für mobile Geräte.

Inhaltsverzeichnis:
- Roadmap
- Lernmodus
- Trainingsmodus / Katalog
- Belohnungssystem
- Statistiken
- über einzelne Module
- über alle Module


## Vorläufige Roadmap

MVP (v0.7): 12.01.2021
- Module anzeigen und Test starten, aus Lokaler DB

v0.8: 01.02.2021
- Katalog, Menü, Login und Firebase DB

v0.9: 19.02.2021
- Statistiken, Abzeichen

v1.0: 05.03.2021
- Bugfixes, Fertige App / Abgabe & Dokumentation

Jeder Meilenstein wird den anderen Teams zum Usability-Test übergeben.


## Workflow
1. Ticket erstellen
2. Ticket zuordnen und in Board auf Status ‘Doing’ setzen
3. Planen wie das Ticket umgesetzt wird
4. Master pullen
5. Neuen Branch erstellen (Name: <Ticketnummer>_<THM-Kürzel>) und auschecken
6. Ticket nach best-practice bearbeiten
7. Code-Review, Test
8. Code dokumentieren und Dokumentation anpassen
9. Mergerequest stellen und pushen

## Mockup:
https://app.moqups.com/BNPfadP6mC/edit/page/a26da5431

## User Stories
**App**
- Der Benutzer bekommt eine tägliche Pushnachricht, um ihn an seine Module zu erinnern. 

**Login:**
- Als neuer Benutzer gebe ich meinen Benutzernamen (oder E-Mail) und ein Passwort an, um mich zu registrieren und einzuloggen.
- Als existierender Benutzer gebe ich meinen Benutzernamen (oder E-Mail) und ein Passwort an, um mich einzuloggen.

**Menü:**
- Als Benutzer klicke ich auf “Statistik”, um Informationen zu meinem Lernprozess zu bekommen.
- Als Benutzer klicke ich auf “Abzeichen”, um meine Errungenschaften zu sehen.
- Als Benutzer klicke ich auf “Logout”, um mich auszuloggen.

**Dashboard:**
- Der Benutzer sieht die letzten beiden zuletzt gelernten Module in der ersten Reihe, damit er nicht lange suchen muss.
- Als Benutzer klicke ich ein Bild an, um das entsprechende Lernmodul starten.
- Als Benutzer klicke ich auf das “Hamburger-Menu”, um das Seitenmenü anzusehen.
- Als Benutzer swipe ich nach unten, um alle Module zu aktualisieren und neue Herunterzuladen.

**Lernmodus:**
- Als Benutzer werden mir eine Frage und 4-6 Antwortmöglichkeiten präsentiert.
- Als Benutzer klicke ich auf eine Frage, um diese auszuwählen. Ich kann mehrere Fragen auswählen und auch wieder abwählen.
- Als Benutzer klicke ich auf den “Bestätigen”-Button, um die Auswahl zu bestätigen. Darauf werden die richtigen Antworten farblich unterlegt.
- Als Benutzer wird mir nach Abschluss eine Statistik zur Lernrunde angezeigt.

**Abzeichen:**
- Als Benutzer sehe ich, welche Abzeichen ich bekommen habe und wie ich diese erreichen kann

**Statistiken:**
- Als Benutzer kann ich ablesen, wieviel Prozent meiner gegebenen Antworten richtig waren.
- Als Benutzer kann ich ablesen, wie viele Module ich angelernt bzw. Abgeschlossen habe.
- (Als Benutzer kann ich meine Lernaktivität einsehen)

**Katalog:**
- Als Benutzer kann ich sehen, wie oft ich eine Frage bereits richtig beantwortet habe.
- Als Benutzer sehe ich an der Reihenfolge, in welcher die Frage aufgelistet werden, welche Fragen ich am öftesten richtig beantwortet habe.
- Als Benutzer habe ich die Möglichkeit, Fragen als “gelernt” bzw. “noch nicht gelernt” zu markieren.
- Als Benutzer habe ich die Möglichkeit, meinen Fragenkatalog zurückzusetzen.


## Ablauf Lernmodus
- Max. 15 Fragen in zufälliger Reihenfolge
- Die 15 Fragen setzen sich zusammen aus:

| Anzahl der Fragen | Score |
| ----------------- | ----- |
| 10                | 0     |
| 2                 | 1-2   |
| 2                 | 3-4   |
| 1                 | 5     |
|                   |       |

  - Score wird um 1 hochgezählt, wenn die Frage richtig beantwortet wurde
  - Und auf 0 zurückgesetzt, wenn sie falsch beantwortet wurde
  - Bei Score = 6 wird die Frage nicht mehr abgefragt
- Jede Fragen, die falsch beantwortet wird, wird am Ende des Tests so lange abgefragt bis man sie richtig beantwortet
- Nach Abschließen des Tests wird die Statistik angezeigt
- Wenn während dem Test Abzeichen erzielt werden, wird dies durch eine Benachrichtigung am oberen Bildschirmrand angezeigt
- Am oberen Bildschirmrand sollen 15 Punkte angezeigt, die die Anzahl der Fragen repäsentiert.
    Jeder Punkt ist entweder rot, grün oder nicht gefüllt je nachdem, ob die Frage falsch, richtig oder noch nicht beantwortet wurde.
    

## Systemarchitektur

**Architektur**
- Projektsprache ist Kotlin
- Architektur ist MVVM

**Dienste:**
- Login-Dienst (kommuniziert mit Backend)
- Refresh-Dienst (aktualisiert Modulliste und Fragen)

**Systeme:**
- Backend (Firebase)
- Lokale Datenbank (Room)
- Frontend (App)
